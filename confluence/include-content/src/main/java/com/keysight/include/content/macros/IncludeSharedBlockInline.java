package com.keysight.include.content.macros;

import com.atlassian.confluence.api.service.content.ContentService;
import com.atlassian.confluence.api.service.content.SpaceService;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.sal.api.transaction.TransactionTemplate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IncludeSharedBlockInline extends IncludeSharedBlockWithReplacement
{
    private static final Logger log = LoggerFactory.getLogger(IncludeSharedBlockInline.class);

    public IncludeSharedBlockInline(
        ContentService contentService,
        PageManager pageManager,
        PermissionManager permissionManager,
        Renderer renderer,
        SpaceManager spaceManager,
        SpaceService spaceService,
        TransactionTemplate transactionTemplate,
        XhtmlContent xhtmlUtils)
    {
        super( contentService, pageManager, permissionManager, renderer, spaceManager, spaceService, transactionTemplate, xhtmlUtils );
    }

    @Override
    protected String filterPageContent(String pageContent)
    {
        int gtIndex = 0;
        pageContent = pageContent.trim();
        if( pageContent.matches( "^<p.*" ) ){
            gtIndex = pageContent.indexOf( ">" );
            pageContent = pageContent.substring( gtIndex + 1, pageContent.length() - 4 );
        }
        else if( pageContent.matches( "^<h[1-6].*" ) ){
            gtIndex = pageContent.indexOf( ">" );
            pageContent = pageContent.substring( gtIndex + 1, pageContent.length() - 5 );
        }
        else if( pageContent.matches( "^<div.*" ) ){
            gtIndex = pageContent.indexOf( ">" );
            pageContent = pageContent.substring( gtIndex + 1, pageContent.length() - 6 );
        }

        return pageContent;
    }

    @Override
    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.INLINE;
    }
}
