package com.keysight.include.content.rest;

import javax.xml.bind.annotation.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class RestResponse {
    private static final Logger log = LoggerFactory.getLogger(RestResponse.class);

    //@XmlAttribute
    //private String messageBody;

    @XmlElement(name = "message-body")
    private String messageBody;

    public RestResponse() {
    }

    public RestResponse(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }
}
