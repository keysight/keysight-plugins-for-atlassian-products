package com.keysight.include.content.helpers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Credentials {
   private static final Logger log = LoggerFactory.getLogger(Credentials.class);

   @XmlElement private String xml;
   public String getXml()           { return xml;     }
   public void   setXml(String xml) { this.xml = xml; }
}


