package com.keysight.include.content.macros;

import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.Space;
import com.atlassian.confluence.api.model.Expansion;
import com.atlassian.confluence.api.model.pagination.SimplePageRequest;
import com.atlassian.confluence.api.service.content.ContentService;
import com.atlassian.confluence.api.service.content.SpaceService;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultXmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactoryFactoryBean;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.service.NotAuthorizedException;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.renderer.ContentIncludeStack;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.confluence.xml.HTMLParagraphStripper;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.Permission;


import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;

import java.util.Map;
import java.util.Calendar;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Optional;
import java.util.TimeZone;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import com.keysight.include.content.helpers.IncludeContentHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class IncludePageWithReplacement implements Macro
{
    private static final Logger log = LoggerFactory.getLogger(IncludePageWithReplacement.class);

    protected ConfluenceUser m_currentUser;
    protected boolean allowAnybodyToViewContent = false;

    protected static final String PAGE_KEY         = "page";
    protected static final String DELIMITER_KEY    = "delimiter";
    protected static final String SUPPRESS_ERRORS  = "suppress-errors";
    protected static final String ERROR            = "Error: ";
    protected static final String NOT_FOUND        = "Page Not Found";
    protected static final String ALREADY_INCLUDED = "The page is already included";
    protected static final String ALLOW_ANONYMOUS_ACCESS = "allowAnonymousAccess";

    protected final ContentService contentService;
    protected final PageManager pageManager;
    protected final PermissionManager permissionManager;
    protected final Renderer renderer;
    protected final SpaceService spaceService;
    protected final SpaceManager spaceManager;
    protected final HTMLParagraphStripper htmlParagraphStripper;
    protected final TransactionTemplate transactionTemplate;
    protected final XhtmlContent xhtmlUtils;

    protected ConversionContext context = null;

    public IncludePageWithReplacement(
        ContentService contentService,
        PageManager pageManager,
        PermissionManager permissionManager,
        Renderer renderer,
        SpaceManager spaceManager,
        SpaceService spaceService,
        TransactionTemplate transactionTemplate,
        XhtmlContent xhtmlUtils)
    {
        this.contentService  = contentService;
        this.pageManager  = pageManager;
        this.permissionManager  = permissionManager;
        this.renderer     = renderer;
        this.spaceManager = spaceManager;
        this.spaceService = spaceService;
        this.transactionTemplate = transactionTemplate;
        this.xhtmlUtils   = xhtmlUtils;

        // This is taken from the Atlassian Page Include Macro
        final XMLOutputFactory xmlOutputFactory;
        try
        {
            xmlOutputFactory = (XMLOutputFactory) new XmlOutputFactoryFactoryBean(true).getObject(); } catch (Exception e) {
            throw new RuntimeException("Error occurred trying to construct a XML output factory", e); // this shouldn't happen
        }
        htmlParagraphStripper = new HTMLParagraphStripper( xmlOutputFactory, new DefaultXmlEventReaderFactory());
    }

    @Override
    public String execute(
        Map<String, String> parameters,
        String body,
        ConversionContext context) throws MacroExecutionException
    {
        String[] targetPage = new String[2];
        String pageTitle;
        String pageContent;
        String replacementText = body;
        m_currentUser = AuthenticatedUserThreadLocal.get();

        Pattern blogPostPattern = Pattern.compile("^/(\\d{4})/(\\d{2})/(\\d{2})/(.*)$");

        this.context = context;

        if (parameters.containsKey(PAGE_KEY))
        {
            pageTitle = parameters.get(PAGE_KEY);

            if( pageTitle.contains( ":" ) )
            {
                targetPage = pageTitle.split( ":", 2 );
            }
            else
            {
                targetPage[0] = context.getSpaceKey();
                targetPage[1] = pageTitle;
            }

            String delimiter = "/";
            if( parameters.containsKey( DELIMITER_KEY ) )
            {
                delimiter = parameters.get( DELIMITER_KEY );
            }

            /*
            Space targetSpace = spaceService.find(new Expansion("name"))
                .withKeys(targetPage[0])
                .fetch()
                .get();

            Optional<Content> contents = null;
            Content contentContainer = null;
            List<ContentType> contentTypes = Arrays.asList(ContentType.PAGE, ContentType.BLOG_POST);
            for (ContentType contentType : contentTypes)
            {
                String targetContentTitle = targetPage[1];
                if(contentType == ContentType.BLOG_POST)
                {
                    //filter out blog dates
                    String[] targetContentTitleParts = targetPage[1].split("/");
                    targetContentTitle = targetContentTitleParts[targetContentTitleParts.length - 1];
                }

                Optional<Content> contentItem = contentService.find()
                    .withType(contentType)
                    .withSpace(targetSpace)
                    .withTitle(targetContentTitle)
                    .fetch();

                if (contentItem.isPresent())
                {
                    contentContainer = contentItem.get();
                    log.warn("Found: " + contentContainer.getTitle() + " as " + targetContentTitle);
                    log.warn("It's a " + contentContainer.getClass().getName());
                    break;
                }
            }
            */

            Matcher blogPostMatcher = blogPostPattern.matcher(targetPage[1]);
            ContentEntityObject page = null;
            if (blogPostMatcher.matches())
            {
                int year = Integer.parseInt(blogPostMatcher.group(1));
                int month = Integer.parseInt(blogPostMatcher.group(2));
                int day = Integer.parseInt(blogPostMatcher.group(3));
                String blogTitle = blogPostMatcher.group(4);

                //Calendar blogDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                Calendar blogDate = Calendar.getInstance();
                blogDate.set(year, month-1, day, 0, 0, 0);
                log.debug(blogDate.toString());

                page = pageManager.getBlogPost( targetPage[0], blogTitle, blogDate );
            }
            else
            {
                page = pageManager.getPage( targetPage[0], targetPage[1] );
            }
            pageContent = getIncludedContent(page, parameters, context, delimiter, replacementText);
        }
        else
        {
             return RenderUtils.blockError(ERROR, NOT_FOUND);
        }

        return filterPageContent(pageContent);
    }

    protected String filterPageContent(String pageContent)
    {
        return pageContent;
    }

    @Override
    public BodyType getBodyType()
    {
        return BodyType.PLAIN_TEXT;
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    protected void setAllowAnybodyToViewContent( boolean bFlag )
    {
        this.allowAnybodyToViewContent = bFlag;
    }
    protected boolean getAllowAnybodyToViewContent()
    {
        return this.allowAnybodyToViewContent;
    }

    // Leveraged from the confluence advanced macros, page include macro
    protected String getIncludedContent(
        ContentEntityObject page,
        Map<String, String> parameters,
        ConversionContext conversionContext,
        String delimiter,
        String replacementText)
    {
        try
        {
            if (page == null)
            {
                log.error("The page ContentEntityObject is null");
                return RenderUtils.blockError(ERROR, NOT_FOUND);
            }
            log.debug("Get content...");
            String result = (String) transactionTemplate.execute(new TransactionCallback()
            {
                public String doInTransaction()
                {
                    return fetchPageContent(page, parameters, conversionContext, delimiter, replacementText);
                };
            });
            return result;
        }
        catch (NotAuthorizedException e)
        {
            log.warn("Not authorized...");
            // Don't let the user know they weren't allowed to see the page.
            return RenderUtils.blockError(ERROR, NOT_FOUND);
        }
        catch (IllegalArgumentException e)
        {
            log.warn("Illegal arguments...");
            return RenderUtils.blockError(ERROR, e.getMessage());
        }
    }

    protected String fetchPageContent(
        ContentEntityObject page,
        Map<String, String> parameters,
        ConversionContext conversionContext,
        String delimiter,
        String replacementText)
    {

        if (ContentIncludeStack.contains(page))
        {
            return RenderUtils.blockError( ERROR, ALREADY_INCLUDED );
        }

        ContentIncludeStack.push(page);
        try
        {
            if (getAllowAnybodyToViewContent() || permissionManager.hasPermission(m_currentUser, Permission.VIEW, page))
            {
                String strippedBody = page.getBodyAsString();
                try
                {
                    strippedBody = htmlParagraphStripper.stripFirstParagraph(page.getBodyAsString());
                }
                catch (XMLStreamException e)
                { }

                strippedBody = IncludeContentHelper.replaceText( strippedBody, delimiter, replacementText );

                DefaultConversionContext context = new DefaultConversionContext(new PageContext(page, conversionContext.getPageContext()));
                return renderer.render(strippedBody, context);
            }
            else
            {
                if (parameters.containsKey(SUPPRESS_ERRORS))
                {
                    return "";
                }
                else
                {
                    return RenderUtils.blockError(ERROR + " You do not have permissions to view this content.", "" );
                }
            }
        }
        finally
        {
            ContentIncludeStack.pop();
        }
    }
}
