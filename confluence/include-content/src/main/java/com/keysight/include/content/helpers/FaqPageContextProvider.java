package com.keysight.include.content.helpers;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.AbstractBlueprintContextProvider;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FaqPageContextProvider extends AbstractBlueprintContextProvider
{
    private static final Logger log = LoggerFactory.getLogger(FaqPageContextProvider.class);

    public FaqPageContextProvider( )
    {
    }

    @Override
    protected BlueprintContext updateBlueprintContext(BlueprintContext context)
    {
        //context.setTitle(pageTitle);
        //context.put("author", author );

        return context;
    }
}
