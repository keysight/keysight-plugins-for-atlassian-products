package com.keysight.keysight.theme.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Context;
import javax.servlet.http.HttpServletRequest;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import org.apache.commons.lang3.StringUtils;

import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.user.User;

//import com.atlassian.sal.api.transaction.TransactionTemplate;
//import com.atlassian.sal.api.transaction.TransactionCallback;
//import com.atlassian.spring.container.ContainerManager;

import com.atlassian.confluence.mail.template.ConfluenceMailQueueItem;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.mail.queue.MailQueueItem;
import static com.atlassian.confluence.mail.template.ConfluenceMailQueueItem.MIME_TYPE_HTML;
import static com.atlassian.confluence.mail.template.ConfluenceMailQueueItem.MIME_TYPE_TEXT;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.keysight.keysight.theme.helpers.MailService;
import com.keysight.keysight.theme.helpers.MailServiceImpl;
import com.atlassian.user.User;

@Path("/contact-form")
public class ContactFormRestService
{
   private static final Logger log = LoggerFactory.getLogger(ContactFormRestService.class);

   private final UserAccessor userAccessor;
   private final SettingsManager settingsManager;
   private final SpaceManager spaceManager;
   private final MailService mailService;
   private final MultiQueueTaskManager taskManager;

   public ContactFormRestService( MultiQueueTaskManager taskManager,
                                  UserAccessor userAccessor,
                                  SettingsManager settingsManager,
                                  SpaceManager spaceManager )
   {
       this.userAccessor               = userAccessor;
       this.settingsManager            = settingsManager;
       this.spaceManager               = spaceManager;
       this.taskManager                = taskManager;

       mailService = new MailServiceImpl( taskManager );
   }

   @POST
   @AnonymousAllowed
   @Produces({MediaType.APPLICATION_JSON})
   @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
   public Response feedbackAction( @FormParam("contact-form-identifier")  String identifier,
		                             @FormParam("contact-form-contact")     String contact,
                                   @FormParam("contact-form-email")       String email,
                                   @FormParam("contact-form-cc-to-email") String ccToEmail,
                                   @FormParam("contact-form-space-key")   String spaceKey,
                                   @FormParam("contact-form-name")        String name,
                                   @FormParam("contact-form-type")        String type,
                                   @FormParam("contact-form-text")        String text ){
      String emailSubject;
      StringBuilder html      = new StringBuilder();
      StringBuilder emailText = new StringBuilder();
      String destinationEmail = "";
      String ccEmail          = "";
      String fromName         = "";
      String fromEmail        = "";
      List<String> destinationEmails = new ArrayList<String>();
      Response response;
      String spaceName        = "";
      boolean toAdmins        = false;
      List<User> admins       = null;
      Space currentSpace      = null;
      String baseUrl   = settingsManager.getGlobalSettings().getBaseUrl();

      if( !StringUtils.isEmpty( spaceKey ) && StringUtils.isEmpty( contact ) ){
         toAdmins = true;
         currentSpace = spaceManager.getSpace( spaceKey );
         if( currentSpace != null ){
            spaceName = currentSpace.getName();
            admins = spaceManager.getSpaceAdmins( currentSpace );
            for( User admin : admins ){
	           destinationEmails.add( admin.getEmail() );
            }
         }
      } else if( contact.matches( ".*@.*" ) ){
         destinationEmails.add( contact );
      } else {
	     destinationEmails.add( userAccessor.getUserByName( contact ).getEmail() );
      }

      if( destinationEmails.size() > 0 ){

         if( !StringUtils.isEmpty(ccToEmail)
             && !StringUtils.isEmpty(email)
	     && ccToEmail.matches( "(?i)true" )
	     && email.matches( ".*@.*" ) ){
            ccEmail = email;
         }

	     if( !StringUtils.isEmpty( name ) && !StringUtils.isEmpty( email ) && email.matches( ".*@.*" ) ){
            fromName = name;
	        fromEmail = email;
	     }

         html.append( "<div class=\"feedback-thankyou\">Thank you for your feedback.</div>" );

         emailSubject = "[" + identifier + "] " + "Webpage feedback of type " + type;
         if(!StringUtils.isEmpty( name )) {
            emailText.append("<p>The user, <b>" + name + "</b>");
            if( !StringUtils.isEmpty( email )) {
               emailText.append(" with the email address " + email + ", " );
            } else {
               emailText.append(", " );
            }
         } else {
            emailText.append("<p>An anonymous user ");
         }
         emailText.append( "has used the <b>Contact Form</b> in the space " );

         if( currentSpace != null){
            emailText.append( "<a href=\"" + baseUrl + "/display/" + spaceKey + "\"></b>"+spaceName+"</b></a> " );
         } else {
            emailText.append( "</b>spaceName</b> " );
         }

         emailText.append( "to send a message" );
         if( toAdmins ) {
            emailText.append(" ");
            emailText.append("to users with space admin permissions.  The message is intended for ");
            emailText.append("the space administrator which may be you or one of the other users with ");
            emailText.append("administrative privileges.</p>" );
            emailText.append("<p>The users listed below  have admin privileges ");
            emailText.append("and were sent a copy of this email.</p>\n");
            emailText.append("<ul>\n");
            for( User admin : admins ){
               emailText.append("<li>");
               emailText.append( admin.getFullName() );
               emailText.append("</li>\n");
            }
            emailText.append("</ul>\n");
         } else {
            emailText.append(".</p>\n");
         }

         emailText.append( "<p>Message body</p>\n");
         emailText.append( "<hr />\n");
         emailText.append( "<pre>" + text + "</pre>\n" );
         emailText.append( "<hr />\n");

         try{
            sendEmail( destinationEmails, ccEmail, fromEmail, fromName, emailSubject, emailText.toString() );
            log.warn( "Sending email to " + destinationEmails.toString() );
         } catch( Exception e ){
            log.warn( "Failed to send email:" + e.getMessage() );
         }

         response = Response.ok( new RestResponse( html.toString() ) ).build();

      } else {
         html.append( "<div class=\"feedback-thankyou\">Error: No contact has been setup to send this feedback to.</div>" );
         response = Response.ok( new RestResponse( html.toString() ) ).build();
      }

      return response;
   }

    public void sendEmail( List<String> toEmails, String ccEmail, String fromEmail, String fromName, String subject, String message) throws Exception
    {
       ConfluenceMailQueueItem mailQueueItem;
       String csvEmails = StringUtils.join( toEmails, "," );

       if( !StringUtils.isEmpty( csvEmails )){

          if( !StringUtils.isEmpty( ccEmail ) && ccEmail.matches( ".*@.*" ) ){
             mailQueueItem = new ConfluenceMailQueueItem( csvEmails, ccEmail, subject, message, MIME_TYPE_HTML);
          } else {
             mailQueueItem = new ConfluenceMailQueueItem( csvEmails, subject, message, MIME_TYPE_HTML);
          }

          if( !StringUtils.isEmpty( fromName ) && !StringUtils.isEmpty( fromEmail ) && fromEmail.matches( ".*@.*" ) ){
             mailQueueItem.setFromName( fromName );
             mailQueueItem.setFromAddress( fromEmail );
          }

          mailService.sendEmail(mailQueueItem);
       }
    }
}
