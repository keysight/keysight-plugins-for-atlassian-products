package com.keysight.keysight.theme.helpers;

// Import required java libraries
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Implements Filter class
public class ImageRedirectFilter implements Filter  {
   private static final Logger log = LoggerFactory.getLogger(ImageRedirectFilter.class);

   public void  init(FilterConfig config) throws ServletException{
   }

   public void  doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain)
                         throws java.io.IOException, ServletException {

      HttpServletRequest httpRequest = (HttpServletRequest) request;
      String servletPath = httpRequest.getServletPath();
      String contextPath = httpRequest.getContextPath();
      String image = servletPath.substring( servletPath.lastIndexOf('/')+1 );

      log.debug( "\n\n" );
      log.debug( "Context Path: " + httpRequest.getContextPath() );
      log.debug( "Servlet Path: " + httpRequest.getServletPath() );
      log.debug( "Path Info: " + httpRequest.getPathInfo() );
      log.debug( "URL: " + httpRequest.getRequestURL() );
      log.debug( "URI: " + httpRequest.getRequestURI() );
      log.debug( "Image: " + image );
      log.debug( "\n\n" );

      // Redirect to the images provided by the plugin...
      HttpServletResponse httpResponse = (HttpServletResponse) response;
      httpResponse.sendRedirect(contextPath+"/download/resources/com.keysight.keysight-theme:keysight-theme-resources/images/" + image);

      // Pass request back down the filter chain
      //chain.doFilter(request,response);
      return;
   }

   public void destroy( ){
   }
}
