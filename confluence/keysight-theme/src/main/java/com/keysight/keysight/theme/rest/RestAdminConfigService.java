package com.keysight.keysight.theme.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserKey;

import com.keysight.keysight.theme.helpers.PluginConfigContainer;
import com.keysight.keysight.theme.helpers.PluginConfigManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/admin-config/")
public class RestAdminConfigService
{
   private static final Logger log = LoggerFactory.getLogger(RestAdminConfigService.class);

   private final UserManager userManager;
   private final PluginSettingsFactory pluginSettingsFactory;
   private final TransactionTemplate transactionTemplate;
   private final PluginConfigManager pluginConfigManager;

   public RestAdminConfigService( PluginConfigManager pluginConfigManager,
                                  PluginSettingsFactory pluginSettingsFactory,
                                  UserManager userManager,
                                  TransactionTemplate transactionTemplate)
   {
      this.userManager = userManager;
      this.pluginConfigManager = pluginConfigManager;
      this.pluginSettingsFactory = pluginSettingsFactory;
      this.transactionTemplate = transactionTemplate;
   }

   @GET
   @Path("configuration")
   @Produces(MediaType.APPLICATION_JSON)
   public Response getConfiguration(@Context HttpServletRequest request) {
       // needed for Confluence Data Center as the config could have been
       // updated by an instance of Confluence other than this one.
       pluginConfigManager.loadFromStorage();

       if (!isAuthorized(request)) {
           return Response.status(Status.UNAUTHORIZED).build();
       }

       PluginConfigContainer pluginConfigContainer = new PluginConfigContainer();
       pluginConfigContainer.urlEncodeAndSetXml( pluginConfigManager.getConfigXml() );

       return Response.ok( pluginConfigContainer ).build();
   }

   @PUT
   @Path("configuration")
   @Consumes(MediaType.APPLICATION_JSON)
   public Response putConfiguration(final PluginConfigContainer pluginConfigContainer,  @Context HttpServletRequest request) {
       // needed for Confluence Data Center as the config could have been
       // updated by an instance of Confluence other than this one.
       pluginConfigManager.loadFromStorage();

       if (!isAuthorized(request)) {
           return Response.status(Status.UNAUTHORIZED).build();
       }

       pluginConfigManager.setConfigXml( pluginConfigContainer.getUrlDecodedXml() );
       return Response.noContent().build();
   }

   // Checks if the user has permissions for the admin page
   // User must be: System Admin OR in user list OR be a member of a group in group list
   private boolean isAuthorized(HttpServletRequest request) {
      UserKey userKey = userManager.getRemoteUserKey(request);

      // Check if logged in and admin
      if (userKey == null) {
         return false;
      } else if (userManager.isSystemAdmin(userKey)) {
         return true;
      } else {
         return false;
      }
   }
}
