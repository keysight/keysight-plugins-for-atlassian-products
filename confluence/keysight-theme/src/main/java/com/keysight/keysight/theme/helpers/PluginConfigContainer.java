package com.keysight.keysight.theme.helpers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PluginConfigContainer {
    private static final Logger log = LoggerFactory.getLogger(PluginConfigContainer.class);

    @XmlElement private String xml;

    //xml = URLDecoder.decode(pluginConfig.getXml(), "UTF-8");
    public String getXml()           { return xml;     }
    public void   setXml(String xml) { this.xml = xml; }

    public void urlEncodeAndSetXml( String xml )
    {
        this.xml = URLEncoder.encode( xml ).replaceAll( "\\+", "%20" );
    }

    public String getUrlDecodedXml()
    {
        return URLDecoder.decode( this.xml );
    }
}

