package com.keysight.keysight.theme.helpers;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KeysightThemeConstants {
    private static final Logger log = LoggerFactory.getLogger(KeysightThemeConstants.class);

    private final static String defaultApiDocumentationServer = "api.is.keysight.com";
    private final static String defaultApiDocumentationServiceUrl = "https://"+defaultApiDocumentationServer+"/cgi-bin/org/of_apps/apiManager/apiManager.cgi";

    public KeysightThemeConstants(){
    }

    public static String getDefaultApiDocumentationServer(){
        return defaultApiDocumentationServer;
    }
    public static String getDefaultApiDocumentationServiceUrl(){
        return defaultApiDocumentationServiceUrl;
    }
}
