(function() {
    var updateMacro = function(macroNode, param) {
        var $macroDiv = jQuery(macroNode);
        AJS.Rte.getEditor().selection.select($macroDiv[0]);
        AJS.Rte.BookmarkManager.storeBookmark();

        var currentParams = {};
        if ($macroDiv.attr("data-macro-parameters")) {
            jQuery.each($macroDiv.attr("data-macro-parameters").split("|"), function(idx, item) {
                var param = item.split("=");
                currentParams[param[0]] = param[1];
            });
        }

        currentParams["checkpoint"] = param;

        var macroRenderRequest = {
            contentId: Confluence.Editor.getContentId(),
            macro: {
                name: "keysight-plc-checkpoint",
                params: currentParams,
                defaultParameterValue: "",
                body : ""
            }
        };

        tinymce.confluence.MacroUtils.insertMacro(macroRenderRequest);
    };

    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('PRECON', function(event, macroNode) {
                updateMacro(macroNode, 'PRECON');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('CON', function(event, macroNode) {
                updateMacro(macroNode, 'CON');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('INV', function(event, macroNode) {
                updateMacro(macroNode, 'INV');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('DEF', function(event, macroNode) {
                updateMacro(macroNode, 'DEF');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('DEV', function(event, macroNode) {
                updateMacro(macroNode, 'DEV');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('EST', function(event, macroNode) {
                updateMacro(macroNode, 'EST');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('HQ', function(event, macroNode) {
                updateMacro(macroNode, 'HQ');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('SQ', function(event, macroNode) {
                updateMacro(macroNode, 'SQ');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('FPR', function(event, macroNode) {
                updateMacro(macroNode, 'FPR');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('MS', function(event, macroNode) {
                updateMacro(macroNode, 'MS');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('SHP', function(event, macroNode) {
                updateMacro(macroNode, 'SHP');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('DIS', function(event, macroNode) {
                updateMacro(macroNode, 'DIS');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('SUP', function(event, macroNode) {
                updateMacro(macroNode, 'SUP');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('CLO', function(event, macroNode) {
                updateMacro(macroNode, 'CLO');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('PRC', function(event, macroNode) {
                updateMacro(macroNode, 'PRC');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('PC', function(event, macroNode) {
                updateMacro(macroNode, 'PC');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('LSR', function(event, macroNode) {
                updateMacro(macroNode, 'LSR');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('RTR', function(event, macroNode) {
                updateMacro(macroNode, 'RTR');
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('DD', function(event, macroNode) {
                updateMacro(macroNode, 'DD');
            });
        }
    });
})();
