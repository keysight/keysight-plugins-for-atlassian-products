(function() {
    var updateMacro = function(macroNode, param) {
        var $macroDiv = jQuery(macroNode);
        AJS.Rte.getEditor().selection.select($macroDiv[0]);
        AJS.Rte.BookmarkManager.storeBookmark();

        var currentParams = {};
        if ($macroDiv.attr("data-macro-parameters")) {
            jQuery.each($macroDiv.attr("data-macro-parameters").split("|"), function(idx, item) {
                var param = item.split("=");
                currentParams[param[0]] = param[1];
            });
        }

        currentParams["color"] = param;

        var macroRenderRequest = {
            contentId: Confluence.Editor.getContentId(),
            macro: {
                name: "status-flag",
                params: currentParams,
                defaultParameterValue: "",
                body : ""
            }
        };

        tinymce.confluence.MacroUtils.insertMacro(macroRenderRequest);
    };
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('StatusFlagGrey', function(event, macroNode) {
                updateMacro(macroNode, "Grey");
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('StatusFlagGreen', function(event, macroNode) {
                updateMacro(macroNode, "Green");
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('StatusFlagYellow', function(event, macroNode) {
                updateMacro(macroNode, "Yellow");
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('StatusFlagRed', function(event, macroNode) {
                updateMacro(macroNode, "Red");
            });
        }
    });
    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('StatusFlagBlue', function(event, macroNode) {
                updateMacro(macroNode, "Blue");
            });
        }
    });
})();
