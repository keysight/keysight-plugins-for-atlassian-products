package com.keysight.test.ecosystem.helpers;

import java.net.URL;
import java.net.MalformedURLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DocumentReference
{
   private static final Logger log = LoggerFactory.getLogger(DocumentReference.class);

   private URL url = null;

   public DocumentReference( URL url ){
      setUrl( url );
   }
   public DocumentReference( String url ) throws MalformedURLException{
      setUrl( url );
   }

   public void setUrl( URL url ){
      this.url = url;
   }
   public void setUrl( String url ) throws MalformedURLException {
      try{
         this.url = new URL(url);
      } catch(MalformedURLException e){ }
   }
   public URL getUrl(){
      return this.url;
   }

   public String getLastComponent(){
      String path = this.url.getPath();
      return path.substring( path.lastIndexOf("/") + 1 );
   }
}
