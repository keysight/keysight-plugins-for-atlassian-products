package com.keysight.glossary.helpers;

import java.util.Date;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.AbstractBlueprintContextProvider;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GlossaryTermContextProvider extends AbstractBlueprintContextProvider
{
    private static final Logger log = LoggerFactory.getLogger(GlossaryTermContextProvider.class);

    public GlossaryTermContextProvider( )
    {
    }

    @Override
    protected BlueprintContext updateBlueprintContext(BlueprintContext context)
    {
        return context;
    }
}
