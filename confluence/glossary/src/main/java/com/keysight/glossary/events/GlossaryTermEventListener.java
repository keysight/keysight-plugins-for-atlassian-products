package com.keysight.glossary.events;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;

import com.atlassian.confluence.plugins.createcontent.api.events.BlueprintPageCreateEvent;
import com.atlassian.plugin.ModuleCompleteKey;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class GlossaryTermEventListener implements DisposableBean
{
    private static final Logger log = LoggerFactory.getLogger(GlossaryTermEventListener.class);

    private static final String GROUP_ID    = "com.keysight";
    private static final String ARTIFACT_ID = "glossary";
    private static final String MODULE      = "glossary-term-blueprint";
    private static final ModuleCompleteKey BLUEPRINT_KEY = new ModuleCompleteKey( GROUP_ID + "." + ARTIFACT_ID, MODULE );

    private final EventPublisher eventPublisher;

    @Autowired
    public GlossaryTermEventListener(EventPublisher eventPublisher)
    {
        this.eventPublisher         = eventPublisher;
        eventPublisher.register(this);
    }


    @EventListener
    public void onPageCreateEvent(BlueprintPageCreateEvent event) {
        ModuleCompleteKey moduleCompleteKey = event.getBlueprintKey();
        if( !BLUEPRINT_KEY.equals( event.getBlueprintKey() ) ){
           return;
        }

        log.debug( event.getBlueprint().toString() );
        log.debug( event.getContext().toString() );
        log.debug( event.getPage().toString() );
    }

    @Override
    public void destroy() throws Exception
    {
        eventPublisher.unregister(this);
    }
}
