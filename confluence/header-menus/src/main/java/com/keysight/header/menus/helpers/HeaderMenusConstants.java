package com.keysight.header.menus.helpers;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HeaderMenusConstants {
    private static final Logger log = LoggerFactory.getLogger(HeaderMenusConstants.class);

    private static List<String> menuLetters = new ArrayList<String>( Arrays.asList( "A", "B", "C", "D", "E" ) );
    private static ArrayList<String> menuFields  = new ArrayList<String>( Arrays.asList( "label", "url", "popupmenu" ) );

    public HeaderMenusConstants(){
    }

    public static List<String> getMenuLetters(){
        return menuLetters;
    }
    public static List<String> getMenuFields(){
        return menuFields;
    }
}
