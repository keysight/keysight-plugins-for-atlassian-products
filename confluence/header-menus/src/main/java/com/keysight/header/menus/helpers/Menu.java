package com.keysight.header.menus.helpers;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Menu {
    private static final Logger log = LoggerFactory.getLogger(Menu.class);

    @XmlElement
    public String label;

    @XmlElement
    public String url;

    @XmlElement
    public String popupmenu;

    private List<PopupMenuItem> popupMenuItems = new ArrayList<PopupMenuItem>();
    private boolean hasPopupmenu = false;

    // Dummy constructor to make JAXB happy
    public Menu() {
    }

    public Menu(Element element) {
        String Buffer;
        try {
            String buffer = element.getElementsByTagName("label").item(0).getTextContent();
            if( StringUtils.isNotBlank( buffer )){
                this.setLabel(new String(Base64.decodeBase64(buffer), "UTF-8"));
            } else {
                this.setLabel(null);
            }
        } catch (Exception e) {
            log.warn("Failed to get the label from the menu XML details.");
        }

        try {
            String buffer = element.getElementsByTagName("url").item(0).getTextContent();
            if( StringUtils.isNotBlank( buffer )){
                this.setUrl(new String(Base64.decodeBase64(buffer), "UTF-8"));
            } else {
                this.setUrl(null);
            }
        } catch (Exception e) {
            log.warn("Failed to get the url from the menu XML details.");
        }

        try {
            String buffer = element.getElementsByTagName("popupmenu").item(0).getTextContent();
            buffer.trim();
            if( StringUtils.isNotEmpty( buffer )){
                this.setPopupmenu(new String(Base64.decodeBase64(buffer), "UTF-8"));
            } else {
                this.setPopupmenu(null);
            }
        } catch (Exception e) {
            log.warn("Failed to get the popupmenu from the menu XML details." + e);
        }
    }

    public void setLabel(String label) {
        if( label != null ){
            label = label.trim();
        }
        this.label = label;
    }
    public String getLabel() {
        return label;
    }

    public void setUrl(String url) {
        if( url != null ){
            url = url.trim();
        }
        this.url = url;
    }
    public String getUrl() {
        if( this.hasPopupMenu() ){
            return "#";
        } else {
            return url;
        }
    }

    public void setPopupmenu(String popupmenu) {
        if( popupmenu != null ){
            popupmenu = popupmenu.trim();
        }
        this.popupmenu = popupmenu;

        if(StringUtils.isNotBlank(popupmenu)){
            this.popupMenuItems = new ArrayList<PopupMenuItem>();
            List<String> items = Arrays.asList(popupmenu.split("\n"));
            for(String item : items){
                this.popupMenuItems.add(new PopupMenuItem(item));
            }
            this.hasPopupmenu = true;
        } else {
            this.popupMenuItems = new ArrayList<PopupMenuItem>();
            this.hasPopupmenu = false;
        }
    }

    public List<PopupMenuItem> getPopupMenuItems(){
        return popupMenuItems;
    }

    public String getPopupmenu() {
        return popupmenu;
    }

    public boolean hasPopupMenu() {
        return this.hasPopupmenu;
    }
}
