package com.keysight.header.menus.helpers;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;

import org.apache.commons.lang3.StringUtils;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PopupMenuContextProvider implements ContextProvider {
    private static final Logger log = LoggerFactory.getLogger(PopupMenuContextProvider.class);
    private final PluginConfigManager pluginConfigManager;
    private static String POPUP_MENU_ITEMS = "popupMenuItems";
    protected String menuLetter = "A";
    private static String MENU_LETTER = "menu-letter";

    protected String getMenuLetter(){
        return this.menuLetter;
    }

    protected void setMenuLetter( String menuLetter ){
        this.menuLetter = menuLetter;
    }

    public PopupMenuContextProvider( PluginConfigManager pluginConfigManager ){
        this.pluginConfigManager = pluginConfigManager;
    }

    public void init(Map<String,String> params) throws PluginParseException
    {
        if( params.containsKey(MENU_LETTER)){
            this.setMenuLetter(params.get(MENU_LETTER));
        }
    }

    public Map<String, Object> getContextMap(Map<String, Object> context) {
        Menu menu = pluginConfigManager.getMenu( this.getMenuLetter() );
        if( menu.hasPopupMenu() ){
            context.put( POPUP_MENU_ITEMS, menu.getPopupMenuItems() );
        } else {
            context.put( POPUP_MENU_ITEMS, new ArrayList<PopupMenuItem>() );
        }
        return context;
    }
}
