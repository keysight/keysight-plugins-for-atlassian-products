package com.keysight.header.menus.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class RestResponse {
    private static final Logger log = LoggerFactory.getLogger(RestResponse.class);

    //@XmlAttribute
    //private String messageBody;

    @XmlElement(name = "message-body")
    private String messageBody;

    public RestResponse() {
    }

    public RestResponse(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }
}
