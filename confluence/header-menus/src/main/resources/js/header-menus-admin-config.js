headerMenusConfigHelper = (function(jQuery) {

    var methods = new Object();
    var url = AJS.contextPath() + "/rest/header-menus/1.0/admin-config/configuration";

    // This dictates whether the stored xml is still valid.
    // Follows the Semver standard (First digit indicates breaking change).
    var schemaVersionString = "1.0.0";
    var schemaVersionArr = schemaVersionString.split(".");
    var menuLetters = ["A", "B", "C", "D", "E"];
    var menuFields = ["label", "url", "popupmenu"];

    methods['saveConfig'] = function() {
        saveConfigInternal();
    }

    methods['loadConfig'] = function() {
        jQuery.ajax({
            url: url,
            dataType: "json"
        }).done(function(pluginConfiguration) {
            var defaultSchema = "<plugin-configuration>\n"
                              + "    <schema-version>"+btoa(schemaVersionString)+"</schema-version>\n";
            menuLetters.forEach(function (menuLetter) {
                defaultSchema += "    <menu-"+menuLetter+">\n";
                menuFields.forEach(function (menuField) {
                    defaultSchema += "       <"+menuField+"></"+menuField+">\n";
                });
                defaultSchema += "    </menu-"+menuLetter+">\n"
            });
            defaultSchema += "</plugin-configuration>";

            // If XML doesn't validate, just give an empty document
            try {
                $xml = jQuery(jQuery.parseXML(decodeURIComponent(pluginConfiguration.xml)));
            } catch(err) {
                if( pluginConfiguration.xml != null){
                   console.error("Failed to parse XML: ", err);
                }
                $xml = jQuery(jQuery.parseXML(defaultSchema));
            }

            // Check for breaking changes in the schema
            if ($xml.find("schema-version").length != 0) {
                var xmlVersion = $xml.find("schema-version").html();
                var xmlMajorVer = xmlVersion.split(".")[0];
                if (xmlMajorVer != schemaVersionArr[0]) {
                    $xml = jQuery(jQuery.parseXML(defaultSchema));
                }
            }

            menuLetters.forEach(function (menuLetter) {
                var buffer;
                if( $xml.find("menu-"+menuLetter).length > 0 ){
                     menuFields.forEach(function (menuField) {
                        buffer = $xml.find("menu-"+menuLetter).find(menuField).html().trim();
                        if( buffer && !(buffer.length === 0) )
                        {
                            try {
                               buffer = atob(buffer);
                            } catch( error ) {
                               alert( "Unable to convert to base64 (" + menuLetter + ", " + menuField + "):" + buffer + "\n" + error);
                            }
                        }
                        jQuery("#menu-"+menuLetter+"-"+menuField).val( buffer );
                     });
                }
            });
        }).fail(function(self, status, error) {
            var loadFlag = AJS.flag({
                type: 'error',
                title: 'Failed to fetch configuration.',
                body: 'Please try again. If problems persist, contact your Confluence administrator.',
                close: 'auto'
            });
        });
    }

    function saveConfigInternal() {
        var xmlString = '<?xml version="1.0" encoding="UTF-8"?>' + "\n" +
            '<plugin-configuration>' + "\n" +
               '<schema-version>'+schemaVersionString+'</schema-version>' + "\n";

        menuLetters.forEach(function (menuLetter) {
            xmlString += "    <menu-"+menuLetter+">\n"
            menuFields.forEach(function (menuField) {
                buffer = jQuery("#menu-"+menuLetter+"-"+menuField).val();
                if( buffer && !(buffer.length === 0) )
                {
                   try {
                        buffer = btoa(buffer);
                   } catch(error) {
                        alert( "Unable to convert from base64 (" + menuLetter + ", " + menuField + "):" + buffer + "\n" + error);
                   }
                   xmlString += "       <"+menuField+">"+buffer+"</"+menuField+">\n";
                } else {
                   xmlString += "       <"+menuField+"></"+menuField+">\n";
                }
            });
            xmlString += "    </menu-"+menuLetter+">\n"
        });
        xmlString += '</plugin-configuration>' + "\n";

        // Validate XML
        try {
            jQuery(jQuery.parseXML(xmlString))
        } catch(err) {
            console.error("Malformed XML!: ", err);
            return;
        }

        jQuery.ajax({
            url: url,
            type: "PUT",
            contentType: "application/json",
            data: '{"xml":"' + encodeURIComponent(xmlString) + '"}',
            processData: false
        }).done(function() {
            var saveSuccessFlag = AJS.flag({
                type: 'success',
                title: 'Success!',
                body: 'Plugin configuration was saved successfully.',
                close: 'auto'
            });
        }).fail(function(self, status, error) {
            var saveFailFlag = AJS.flag({
                type: 'error',
                title: 'Failed to save configuration.',
                body: 'Please try again. If problems persist, contact your Confluence administrator.',
                close: 'auto'
            });
        });
    }

    return methods;
})(jQuery);

AJS.toInit(function() {

    jQuery("#save-config").click(function(e) {
        e.preventDefault();
        headerMenusConfigHelper.saveConfig();
    });

    headerMenusConfigHelper.loadConfig();
});
