package com.keysight.html.elements.macros;

import java.util.Map;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Block implements Macro
{
    private static final Logger log = LoggerFactory.getLogger(Block.class);

    private static final String WIDTH_KEY     = "width";
    private static final String ALIGNMENT_KEY = "alignment";
    private static final String LEFT          = "Left";
    private static final String CENTER        = "Center";
    private static final String RIGHT         = "Right";
    private static final String CLASS_KEY     = "class";

    public Block()
    {
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context)
            throws MacroExecutionException
    {
        String tableWidth     = "";
	    StringBuilder html    = new StringBuilder();
        String alignment      = null;
        boolean bAlignmentSet = false;
        String containerClass = "";
	    String blockClass     = "";
        String customClass    = "";
	    String style          = "";
	    String spacer1        = "";
        String spacer2        = "";

        if( parameters.containsKey( CLASS_KEY ) ){
            customClass = parameters.get( CLASS_KEY );
        }

        if( parameters.containsKey( ALIGNMENT_KEY ) ){
           alignment = parameters.get( ALIGNMENT_KEY );
	       bAlignmentSet = true;

	       if( alignment.matches( RIGHT ) ){
              containerClass = "keysight-container-block-right-justify";
              blockClass     = "keysight-inner-block-right-aligned";
	       } else if( alignment.matches( CENTER ) ){
              containerClass = "keysight-container-block-center-justify";
              blockClass     = "keysight-inner-block-center-aligned";
	       } else {
              containerClass = "keysight-container-block-left-justify";
              blockClass     = "keysight-inner-block-left-aligned";
	       }

	       html.append( "<div class=\"keysight-container-block " + containerClass + "\">\n" );
	    }

        if( parameters.containsKey( WIDTH_KEY ) ){
           style = "style=\"width:"+parameters.get( WIDTH_KEY )+"\"";
	    }

        if( !StringUtils.isEmpty( customClass ) || !StringUtils.isEmpty( blockClass )) {
            spacer1 = " ";
        }

	    if( !StringUtils.isEmpty( customClass ) && !StringUtils.isEmpty( blockClass )){
           spacer2 = " ";
        }

	    html.append( "   <div class=\"keysight-inner-block" + spacer1 + customClass + spacer2 + blockClass + "\" " + style + ">\n" );
	    html.append( body );
        html.append( "   </div>\n" );

	    if( bAlignmentSet ){
	       html.append( "<div style=\"clear:both\"></div>\n" );
	       html.append( "</div>\n" );
	    }

	    return html.toString();
    }

    @Override
    public BodyType getBodyType()
    {
        return BodyType.RICH_TEXT;
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }
}
