package com.keysight.html.elements.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.setup.settings.SettingsManager;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuiDropDownButton extends AuiButton
{
   private static final Logger log = LoggerFactory.getLogger(AuiDropDownButton.class);

   public AuiDropDownButton( AttachmentManager attachmentManager,
                             PageManager pageManager,
                             SettingsManager settingsManager,
                             SpaceManager spaceManager,
                             VelocityHelperService velocityHelperService)
   {
      super( attachmentManager, pageManager, settingsManager, spaceManager, velocityHelperService );
   }

   @Override
   public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException
   {
      String template = "/com/keysight/html-elements/templates/aui-dropdown-button.vm";
      Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();

      velocityContext.put( BUTTON_TEXT_KEY, parameters.get(BUTTON_TEXT_KEY) );
      if( parameters.containsKey( BUTTON_STYLE_KEY ) ){
         if( parameters.get( BUTTON_STYLE_KEY ).equals( "Primary" ) ){
            velocityContext.put( BUTTON_STYLE_KEY, "aui-button-primary" );
         } else if( parameters.get( BUTTON_STYLE_KEY ).equals( "Link-Style" ) ){
            velocityContext.put( BUTTON_STYLE_KEY, "aui-button-link" );
         } else if( parameters.get( BUTTON_STYLE_KEY ).equals( "Subtle" ) ){
            velocityContext.put( BUTTON_STYLE_KEY, "aui-button-subtle" );
         }
      }

      // examine and prepare the body...
      body = body.trim();
      if( !( body.matches( "<ul.*" ) && body.matches( ".*</ul>" ) ) ){
         body = "<ul class=\"aui-list-truncate\"><li><a href=\"#\">Improper body.  Needs to be a single unordered (bullet) list of anchors</li></ul>";
      } else {
         body.replaceFirst( "<ul>", "<ul class=\"aui-list-truncate\">" );
      }

      velocityContext.put( DROPDOWN_MENU_ID_KEY, generateId() );
      velocityContext.put( RENDERED_BODY_KEY, body );

      return velocityHelperService.getRenderedTemplate(template, velocityContext);
   }

   @Override
   public BodyType getBodyType()
   {
      return BodyType.RICH_TEXT;
   }
}
