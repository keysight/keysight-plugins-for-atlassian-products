package com.keysight.html.elements.macros;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnonymousOnlyBlock implements Macro
{
    private static final Logger log = LoggerFactory.getLogger(AnonymousOnlyBlock.class);

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context)
            throws MacroExecutionException
    {
        boolean bAnonymousUser = false;

        if( AuthenticatedUserThreadLocal.get() == null ){
            return body;
        } else {
            return "";
        }
    }

    @Override
    public BodyType getBodyType()
    {
        return BodyType.RICH_TEXT;
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }
}
