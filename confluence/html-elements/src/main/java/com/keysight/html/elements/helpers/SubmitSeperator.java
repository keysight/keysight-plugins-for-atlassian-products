package com.keysight.html.elements.helpers;

import java.util.ArrayList;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SubmitSeperator
{
   private static final Logger log = LoggerFactory.getLogger(SubmitSeperator.class);

   private String seperator = null;

   public SubmitSeperator(){
   }

   public SubmitSeperator( String seperator ){
      this.seperator = seperator;
   }

   public void setSeperator( String seperator ){
      this.seperator = seperator;
   }
   @HtmlSafe
   public String getSeperator(){
      return this.seperator;
   }
}
