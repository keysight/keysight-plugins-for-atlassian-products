AJS.toInit(function(){
   AJS.tabs.setup();

   // Without this hook, Confluence will scroll the page
   // so that the top of the tab is at the top of the screen.
   // This code triggers the tab change, than stops propagation
   // or the event so the page is not scrolled.
   $(".keysight-tab-menu-item-anchor").click(function(e){
      AJS.tabs.change(jQuery(this), e)
      e.preventDefault()
      e.stopImmediatePropagation();
   });
});
