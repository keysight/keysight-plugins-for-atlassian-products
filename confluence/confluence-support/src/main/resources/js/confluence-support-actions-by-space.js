var actionsBySpaceController = (function (jQuery) { // this closure helps us keep our variables to ourselves.
    // form the URL
    var methods = new Object();
    var getSpacesRestUrl = AJS.contextPath() + "/rest/confluence-support/1.0/admin/get-spaces";
    var fixPagesBySpaceRestUrl = AJS.contextPath() + "/rest/confluence-support/1.0/admin/fix-titleless-pages-in-space";
    var onePageRestUrl = AJS.contextPath() + "/rest/confluence-support/1.0/admin/fix-titleless-page";
    var emptyTrashBySpaceRestUrl = AJS.contextPath() + "/rest/confluence-support/1.0/admin/empty-trash-in-space";
    var replaceTextBySpaceRestUrl = AJS.contextPath() + "/rest/confluence-support/1.0/admin/replace-text-in-space";
    var testReplaceTextRestUrl = AJS.contextPath() + "/rest/confluence-support/1.0/admin/test-replace-text";
    var validateGroupsRestUrl = AJS.contextPath() + "/rest/confluence-support/1.0/admin/validate-groups";
    var fixAnonymousAccessIssuesBySpaceRestUrl = AJS.contextPath() + "/rest/confluence-support/1.0/admin/fix-anonymous-access-issues";
    var spinning = false;
    var mode = 'dry-run';
    var dryRun = true;
    var spaceList;
    var currentSpaceIndex;
    var runAction;
    var replaceConfirmationDialogIsOpen = false;
    var replacementPattern;
    var replacementText;

    function updateProgressContainer(text){
       jQuery("#actions-by-space-progress-container").html( "<p>" + text + "</p>" );
    }

    function spinnerOn(){
       jQuery('#keysight-fix-unnamed-pages-spinner').spin();
       spinning = true;
    }

    function spinnerOff(){
       jQuery('#keysight-fix-unnamed-pages-spinner').spinStop();
       spinning = false;
    }

    methods["initializePage"] = function() {
       jQuery("#dry-run-replace-button").on( "click", function(e){ fixAllPages( e, "dry-run-replace-text" ); } );
       jQuery("#replace-text-button").on( "click", function(e){ confirmReplaceTextOnAllPages( e, "replace-text" ); } );
       jQuery("#test-replace-text-button").on( "click", function(e){ testReplaceText( e ); } );

       jQuery("#fix-title-list-pages-button").on( "click", function(e){ fixAllPages( e, "list-titleless-pages" ); } );
       jQuery("#fix-title-fix-pages-button").on( "click", function(e){ fixAllPages( e, "fix-titleless-pages" ); } );

       jQuery("#list-space-trash-button").on( "click", function(e){ fixAllPages( e, "list-space-trash" ); } );
       jQuery("#empty-space-trash-button").on( "click", function(e){ fixAllPages( e, "empty-space-trash" ); } );

       jQuery("#list-anonymous-access-issues-button").on( "click", function(e){ validateFixAnonymousIssues( e, "list-anonymous-access-issues" ); } );
       jQuery("#fix-anonymous-access-issues-button").on( "click", function(e){ validateFixAnonymousIssues( e, "fix-anonymous-access-issues" ); } );
    }

    function validateFixAnonymousIssues(e, setmode)
    {
       preventIt(e);
       runAction = setmode;
       var allUsersGroups = jQuery("#fix-anonymous-access-all-users-groups").val();

       if( !allUsersGroups ){
          jQuery("#error-for-fix-anonymous-all-users-groups").html("One or more groups must be specified");
       } else {

          var groups = allUsersGroups.split(",");
          for( var i = 0; i < groups.length; i++ ){
             groups[i] = groups[i].trim();
          }
          var args = { groups:groups };

          jQuery.ajax({
             url: validateGroupsRestUrl,
             dataType: "json",
             data: args
          }).done(function(data) {
             if( data[0] == "OK" ){
                jQuery("#error-for-fix-anonymous-all-users-groups").html( "" );
                getSpaces();
             } else {
                jQuery("#error-for-fix-anonymous-all-users-groups").html( data[0] );
             }
          }).fail(function(self,status,error){
             alert( error );
          });
       }

    }

    function confirmReplaceTextOnAllPages(e, setmode)
    {
       preventIt(e);
       runAction = setmode;
       var originalText = jQuery("#find-and-replace-original-text").val();

       if( !originalText ){
          jQuery("#error-for-find-and-replace-original-text").html("The original text cannot be empty.");
       } else {
          jQuery("#error-for-find-and-replace-original-text").html("");
          if( !replaceConfirmationDialogIsOpen ){
             if( jQuery("#replace-page-confirmation-dialog" ).length == 0 ){
                jQuery( "body" ).append( Keysight.Confluence.Support.Soy.Templates.replaceTextConfirmation(
                        { originalText: originalText,
                          replacementText: jQuery("#find-and-replace-replacement-text").val(),
                          closeMethod:"actionsBySpaceController.closeConfirmReplaceTextOnAllPages();",
                          openMethod:"actionsBySpaceController.executeReplaceTextOnAllPages();" } ));
             }
             replaceConfirmationDialogIsOpen = true;
             AJS.dialog2( "#replace-page-confirmation-dialog" ).show();
          }
       }
    }

    function testReplaceText(e){
       preventIt(e);
       var args = { "original-text":  jQuery("#find-and-replace-sample-text").val(),
                    "pattern-text": jQuery("#find-and-replace-original-text").val(),
                    "replacement-text": jQuery("#find-and-replace-replacement-text").val()
       }

       jQuery.ajax({
          url: testReplaceTextRestUrl,
          dataType: "json",
          data: args
       }).done(function(data) {
          alert( data[0] );
       }).fail(function(self,status,error){
          alert( error );
       });
    }

    methods["executeReplaceTextOnAllPages"] = function()
    {
        methods.closeConfirmReplaceTextOnAllPages();
        getSpaces();
    }

    methods["closeConfirmReplaceTextOnAllPages"] = function()
    {
       if( replaceConfirmationDialogIsOpen ){
          AJS.dialog2( "#replace-page-confirmation-dialog" ).hide();
          replaceConfirmationDialogIsOpen = false;
       }
    }

    function getSpaces()
    {
       var args;

       // clear the space list
       jQuery(".actions-by-space-space-list-container").html( "<ul class=\"actions-by-space-space-list\"></ul>" );

       if( runAction === "list-titleless-pages" || runAction === "fix-titleless-pages" ){
          args = { "space-categories" : jQuery("#fix-title-space-categories").val() };
       } else if( runAction === "list-space-trash" || runAction === "empty-space-trash" ){
          args = { "space-categories" : jQuery("#empty-trash-space-categories").val() };
       } else if( runAction === "dry-run-replace-text" || runAction === "replace-text" ){
          args = { "space-key" : jQuery("#find-and-replace-space-key").val(),
                   "space-categories" : jQuery("#find-and-replace-space-categories").val() };
       } else if( runAction === "list-anonymous-access-issues" || runAction === "fix-anonymous-access-issues" ){
          args = { "space-key" : jQuery("#fix-anonymous-access-space-key").val(),
                   "space-categories" : jQuery("#fix-anonymous-access-space-categories").val() };
       }

       jQuery.ajax({
          url: getSpacesRestUrl,
          dataType: "json",
          data: args
       }).done(function(data) {
          spaceList = data;

          for( i = 0; i < spaceList.length; i++ ){
             var niceSpaceKey = spaceList[i].spaceKey.replace("~", "--tilda--");
             jQuery(".actions-by-space-space-list").append( "<li class=\"actions-by-space-space-entry\" id=\"space-key-" + niceSpaceKey + "\">"
                                                     + spaceList[i].spaceName + " (" + spaceList[i].spaceKey + ")</li>\n" );
          }

          jQuery(".actions-by-space-results-list-container").html( "<ul class=\"actions-by-space-results-list\"></ul>" );
          currentSpaceIndex = 0;
          if( runAction === "list-titleless-pages" || runAction === "fix-titleless-pages" ){
             listTitlelessPages();
          } else if( runAction === "list-space-trash" || runAction === "empty-space-trash" ){
             listSpaceTrash();
          } else if( runAction === "dry-run-replace-text" || runAction === "replace-text" ){
             listReplaceText();
          } else if( runAction === "list-anonymous-access-issues" || runAction === "fix-anonymous-access-issues" ){
             listAnonymousAccessIssues();
          }

       }).fail(function(self,status,error){
          alert( error );
       });
    }

    function listAnonymousAccessIssues()
    {
       if( currentSpaceIndex < spaceList.length ){
          var args = { "space-key":spaceList[currentSpaceIndex].spaceKey };
          var niceSpaceKey = spaceList[currentSpaceIndex].spaceKey.replace("~", "--tilda--");

          if( runAction === "fix-anonymous-access-issues" ){
             args.mode = "fix-issues";
          }

          var groups = jQuery("#fix-anonymous-access-all-users-groups").val().split(",");
          for( var i = 0; i < groups.length; i++ ){
             groups[i] = groups[i].trim();
          }

          args["groups"] = groups;



          updateProgressContainer( "Scanning " + spaceList[currentSpaceIndex].spaceName);
          jQuery(".actions-by-space-active").addClass( "actions-by-space-visited" ).removeClass( "actions-by-space-active" );
          jQuery("#space-key-"+niceSpaceKey).addClass( "actions-by-space-active" );
       
          jQuery.ajax({
             url: fixAnonymousAccessIssuesBySpaceRestUrl,
             dataType: "json",
             data: args
          }).done(function(data) {
             for( i = 0; i < data.length; i++ ){
                jQuery(".actions-by-space-results-list").append( "<li>" + data[i] + "</li>\n" );
                jQuery(".fix-anonymous-access-issue-in-space-anchor").off();
                jQuery(".fix-anonymous-access-issue-in-space-anchor").on("click", function(e){ fixAnonymousAccessIssueInSpace(e); } );
             }
          }).fail(function(self,status,error){
             alert( error );
          });

          currentSpaceIndex++;
          listAnonymousAccessIssues();
       } else {
          updateProgressContainer( "" );
          jQuery(".actions-by-space-active").addClass( "actions-by-space-visited" ).removeClass( "actions-by-space-active" );
       }
    }

    function listReplaceText()
    {
       if( currentSpaceIndex < spaceList.length ){
          var args = { "space-key":spaceList[currentSpaceIndex].spaceKey };

          if( runAction === "replace-text" ){
             args.mode = "replace-text";
          }
          replacementPattern = jQuery("#find-and-replace-original-text").val();
          replacementText    = jQuery("#find-and-replace-replacement-text").val();

          args["pattern-text"] = replacementPattern;
          args["replacement-text"] = replacementText;

          updateProgressContainer( "Scanning " + spaceList[currentSpaceIndex].spaceName);
          jQuery(".actions-by-space-active").addClass( "actions-by-space-visited" ).removeClass( "actions-by-space-active" );
          jQuery("#space-key-"+spaceList[currentSpaceIndex].spaceKey).addClass( "actions-by-space-active" );
       
          jQuery.ajax({
             url: replaceTextBySpaceRestUrl,
             dataType: "json",
             data: args
          }).done(function(data) {
             for( i = 0; i < data.length; i++ ){
                jQuery(".actions-by-space-results-list").append( "<li>" + data[i] + "</li>\n" );
                jQuery(".replace-text-in-space-anchor").off();
                jQuery(".replace-text-in-space-anchor").on("click", function(e){ replaceTextInSpace(e); } );
             }
          }).fail(function(self,status,error){
             alert( error );
          });

          currentSpaceIndex++;
          listReplaceText();
       } else {
          updateProgressContainer( "" );
          jQuery(".actions-by-space-active").addClass( "actions-by-space-visited" ).removeClass( "actions-by-space-active" );
       }
    }

    function listTitlelessPages()
    {
       if( currentSpaceIndex < spaceList.length ){
          var args = { "space-key":spaceList[currentSpaceIndex].spaceKey };

          if( runAction === "fix-titleless-pages" ){
             args.mode = "fix-titleless-pages";
          }

          updateProgressContainer( "Scanning " + spaceList[currentSpaceIndex].spaceName);
          jQuery(".actions-by-space-active").addClass( "actions-by-space-visited" ).removeClass( "actions-by-space-active" );
          jQuery("#space-key-"+spaceList[currentSpaceIndex].spaceKey).addClass( "actions-by-space-active" );
       
          jQuery.ajax({
             url: fixPagesBySpaceRestUrl,
             dataType: "json",
             data: args
          }).done(function(data) {
             for( i = 0; i < data.length; i++ ){
                jQuery(".actions-by-space-results-list").append( "<li>" + data[i] + "</li>\n" );
                jQuery(".keysight-fix-page-anchor").off();
                jQuery(".keysight-fix-page-anchor").on("click", function(e){ fixUnnamedPage(e); } );
             }
          }).fail(function(self,status,error){
             alert( error );
          });

          currentSpaceIndex++;
          listTitlelessPages();
       } else {
          updateProgressContainer( "" );
          jQuery(".actions-by-space-active").addClass( "actions-by-space-visited" ).removeClass( "actions-by-space-active" );
       }
    }

    function listSpaceTrash()
    {
       if( currentSpaceIndex < spaceList.length ){
          var args = { "space-key":spaceList[currentSpaceIndex].spaceKey };

          if( runAction === "empty-space-trash" ){
             args.mode = "empty-space-trash";
          }

          updateProgressContainer( "Scanning " + spaceList[currentSpaceIndex].spaceName);
          jQuery(".actions-by-space-active").addClass( "actions-by-space-visited" ).removeClass( "actions-by-space-active" );
          jQuery("#space-key-"+spaceList[currentSpaceIndex].spaceKey).addClass( "actions-by-space-active" );
       
          jQuery.ajax({
             url: emptyTrashBySpaceRestUrl,
             dataType: "json",
             data: args
          }).done(function(data) {
             for( i = 0; i < data.length; i++ ){
                jQuery(".actions-by-space-results-list").append( "<li>" + data[i] + "</li>\n" );
                jQuery(".empty-trash-in-space-anchor").off();
                jQuery(".empty-trash-in-space-anchor").on("click", function(e){ emptyTrashInSpace(e); } );
             }
          }).fail(function(self,status,error){
             alert( error );
          });

          currentSpaceIndex++;
          listSpaceTrash();
       } else {
          updateProgressContainer( "" );
          jQuery(".actions-by-space-active").addClass( "actions-by-space-visited" ).removeClass( "actions-by-space-active" );
       }
    }

    function fixAnonymousAccessIssueInSpace( e ){
       preventIt( e );

       var group = jQuery(e.target).attr( "fix-anonymous-access-issues-for-group" );
       var permission = jQuery(e.target).attr( "fix-anonymous-access-issues-for-permission" );
       var spaceKey = jQuery(e.target).attr( "fix-anonymous-access-issues-in-space-with-key" );
       var args = { "space-key" : spaceKey, 
                    "return-space-key" : "true", 
                    "mode" : "fix-issues",
                    "groups" : [group],
                    "permissions" : [permission] };

       jQuery.ajax({
          url: fixAnonymousAccessIssuesBySpaceRestUrl,
          dataType: "json",
          data: args
       }).done(function(data) {
          if( data.length >= 4 && data.length % 4 == 0 ){
             for( var i = 0; i < data.length; i += 4 ){
                var id = "#fix-anonymous-access-issues-for-"+data[i]+"-for-group-"+data[i+1]+"-in-space-"+data[i+2];
                jQuery( id ).html( data[i+3] );
             }
          }
       }).fail(function(self,status,error){
          alert( error );
       });
    }

    function replaceTextInSpace( e ){
       preventIt( e );
       var spaceKey = jQuery(e.target).attr( "replace-text-in-space-with-key" );
       var args = { "space-key" : spaceKey, 
                    "return-space-key" : "true", 
                    "mode" : "replace-text",
                    "pattern-text" : replacementPattern,
                    "replacement-text" : replacementText };

       jQuery.ajax({
          url: replaceTextBySpaceRestUrl,
          dataType: "json",
          data: args
       }).done(function(data) {
          if( data.length == 2 ){
             jQuery( "#replace-text-in-" + data[0] ).html( data[1] );
          }
       }).fail(function(self,status,error){
          alert( error );
       });
    }

    function emptyTrashInSpace( e ){
       preventIt( e );
       var spaceKey = jQuery(e.target).attr( "empty-trash-in-space-with-key" );
       var args = { "space-key" : spaceKey, "return-space-key" : "true", "mode" : "empty-space-trash" };

       jQuery.ajax({
          url: emptyTrashBySpaceRestUrl,
          dataType: "json",
          data: args
       }).done(function(data) {
          if( data.length == 2 ){
             jQuery( "#empty-trash-in-" + data[0] ).html( data[1] );
          }
       }).fail(function(self,status,error){
          alert( error );
       });
    }

    function fixUnnamedPage( e ){
       preventIt( e );
       var pageId = jQuery(e.target).attr( "id-of-page-to-fix" );
       var args = { "page-id" : pageId };

       jQuery.ajax({
          url: onePageRestUrl,
          dataType: "json",
          data: args
       }).done(function(data) {
          if( data.length == 2 ){
             jQuery( "#fixpage-" + data[0] ).html( data[1] );
          }
       }).fail(function(self,status,error){
          alert( error );
       });
    }

    function fixAllPages( e, setmode ) {
       preventIt( e );
       runAction = setmode;

       if( (setmode === "replace-text" || setmode === "dry-run-replace-text") && !jQuery("#find-and-replace-original-text").val() )
       {
          jQuery("#error-for-find-and-replace-original-text").html("The original text cannot be empty.");
       } else {
          jQuery("#error-for-find-and-replace-original-text").html("");
          getSpaces();
       }
    }

    function preventIt( e ){
       e.preventDefault();
       e.stopPropagation();
    }


    return methods;
})(jQuery);

AJS.toInit(function() {
   actionsBySpaceController.initializePage();
});


