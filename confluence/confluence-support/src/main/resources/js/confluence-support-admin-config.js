confluenceSupportConfigHelper = (function (jQuery) {

    var methods = new Object();
    var url = AJS.contextPath() + "/rest/confluence-support/1.0/admin-config/configuration";
    var allUsersGroup = "all-users-group";
    var onSpaceCreationNotificationEmailAddress = "on-space-creation-notification-email-address";

    methods['addGroup'] = function(){
       if( jQuery("#group-list").length == 0 ){
           jQuery("#group-list-container").html( "<ul id=\"group-list\"></ul>" );
       }

       appendGroup( jQuery("#new-group-name").val() );
       jQuery("#new-group-name").val("");

       saveConfig();
    }

    methods['addEmail'] = function(){
       if( jQuery("#email-list").length == 0 ){
           jQuery("#email-list-container").html( "<ul id=\"email-list\"></ul>" );
       }

       appendEmail( jQuery("#new-email-address").val() );
       jQuery("#new-email-address").val("");

       saveConfig();
    }

    methods['removeEntry'] = function(e){
       e.preventDefault();
       jQuery(e.currentTarget).parent().remove();

       if( jQuery(".saved-group").length == 0 ){
           jQuery("#group-list-container").html( "<div class=\"none-saved\">No saved groups</div>" );
       }

       if( jQuery(".saved-address").length == 0 ){
              jQuery("#email-list-container").html( "<div class=\"none-saved\">No saved addresses.</div>" );
       }

       saveConfig();
    }

    methods['loadConfig'] = function(){

       jQuery.ajax({
           url: url,
           dataType: "json"
       }).done(function(pluginConfiguration) { 

           $xml = jQuery( jQuery.parseXML( decodeURIComponent(pluginConfiguration.xml) ) );

           jQuery("#group-list-container").children().first().remove();
           jQuery("#email-list-container").children().first().remove();


           var groups = $xml.find( allUsersGroup );
           if( groups.length > 0 ){
              jQuery("#group-list-container").html( "<ul id=\"group-list\"></ul>" );
              $xml.find( allUsersGroup ).each( function( index ) {
                 appendGroup( jQuery(this).html() );
              });
           } else {
              jQuery("#group-list-container").html( "<div class=\"none-saved\">No saved groups</div>" );
           }

           var addresses = $xml.find( onSpaceCreationNotificationEmailAddress );
           if( addresses.length > 0 ){
              jQuery("#email-list-container").html( "<ul id=\"email-list\"></ul>" );
              $xml.find( onSpaceCreationNotificationEmailAddress ).each( function( index ) {
                 appendEmail( jQuery(this).html() );
              });
           } else {
              jQuery("#email-list-container").html( "<div class=\"none-saved\">No saved addresses.</div>" );
           }

       }).fail(function(self,status,error){
          alert( error );
       });

    }

    function saveConfig(){
       var groupconfig = new Array();
       var emailconfig = new Array();

       jQuery(".saved-group").each( function( index ) {
          groupconfig.push( '<all-users-group>' + jQuery(this).html() + '</all-users-group>' );
       });

       jQuery(".saved-address").each( function( index ) {
          emailconfig.push( '<on-space-creation-notification-email-address>' + jQuery(this).html() + '</on-space-creation-notification-email-address>' );
       });

       var xmlString = '<?xml version="1.0" encoding="UTF-8"?>' + "\n"
                     + '<plugin-configuration>' + "\n"
                     + '   <all-users-groups>' + "\n"
                     + groupconfig.join( "\n" )  
                     + '   </all-users-groups>' + "\n"
                     + '   <on-space-creation-notification-email-addresses>' + "\n"
                     + emailconfig.join( "\n" )  
                     + '   </on-space-creation-notification-email-addresses>' + "\n"
                     + '</plugin-configuration>' + "\n";

       jQuery.ajax({
          url: url,
          type: "PUT",
          contentType: "application/json",
          data: '{"xml":"' + encodeURIComponent(xmlString) + '"}',
          processData: false
       }).done(function () { 
       }).fail(function (self, status, error) { alert(error); 
       });
    }

    function appendEmail( emailAddress )
    {
       jQuery("#email-list").append( "<li>"
                                  + "<span class=\"saved-address\">"+emailAddress+"</span>" + "&nbsp;<a href=\"#\" onclick=\"confluenceSupportConfigHelper.removeEntry(event)\">[remove]</a>"
                                  +"</li>\n" );
    }

    function appendGroup( groupName )
    {
       jQuery("#group-list").append( "<li>"
                                  + "<span class=\"saved-group\">"+groupName+"</span>" + "&nbsp;<a href=\"#\" onclick=\"confluenceSupportConfigHelper.removeEntry(event)\">[remove]</a>"
                                  +"</li>\n" );
    }

    return methods;
})(jQuery);

AJS.toInit(function() {

   jQuery("#add-group").click(function(e) {
      e.preventDefault();
      confluenceSupportConfigHelper.addGroup();
   });

   jQuery("#add-email").click(function(e) {
      e.preventDefault();
      confluenceSupportConfigHelper.addEmail();
   });

   confluenceSupportConfigHelper.loadConfig();
});
