(function() {
    var updateMacro = function(macroNode, param) {
        var $macroDiv = jQuery(macroNode);
        AJS.Rte.getEditor().selection.select($macroDiv[0]);
        AJS.Rte.BookmarkManager.storeBookmark();

        var currentParams = {};
        if ($macroDiv.attr("data-macro-parameters")) {
            jQuery.each($macroDiv.attr("data-macro-parameters").split("|"), function(idx, item) {
                var param = item.split("=");
                currentParams[param[0]] = param[1];
            });
        }

        currentParams["guidance"] = param;

        var macroRenderRequest = {
            contentId: Confluence.Editor.getContentId(),
            macro: {
                name: "guideline-inline",
                params: currentParams,
                defaultParameterValue: "",
                body : ""
            }
        };

        tinymce.confluence.MacroUtils.insertMacro(macroRenderRequest);
    };


    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('InlineGuidelineDo', function(event, macroNode) {
                updateMacro(macroNode, "Do");
            });
        }
    });

    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('InlineGuidelineConsider', function(event, macroNode) {
                updateMacro(macroNode, "Consider");
            });
        }
    });

    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('InlineGuidelineAvoid', function(event, macroNode) {
                updateMacro(macroNode, "Avoid");
            });
        }
    });

    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('InlineGuidelineDoNot', function(event, macroNode) {
                updateMacro(macroNode, "Do Not");
            });
        }
    });
})();

(function() {
    var updateMacro = function(macroNode, param) {
        var $macroDiv = jQuery(macroNode);
        AJS.Rte.getEditor().selection.select($macroDiv[0]);
        AJS.Rte.BookmarkManager.storeBookmark();

        var currentParams = {};
        if ($macroDiv.attr("data-macro-parameters")) {
            jQuery.each($macroDiv.attr("data-macro-parameters").split("|"), function(idx, item) {
                var param = item.split("=");
                currentParams[param[0]] = param[1];
            });
        }

        currentParams["guidance"] = param;

        var t = tinymce.confluence.macrobrowser;
        var macroRenderRequest = {
            contentId: Confluence.Editor.getContentId(),
            macro: {
                name: "guideline-block",
                params: currentParams,
                defaultParameterValue: "",
                body : $macroDiv.find(".wysiwyg-macro-body").html()
            }
        };

        tinymce.confluence.MacroUtils.insertMacro(macroRenderRequest);
    };

    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('BlockGuidelineAuto', function(event, macroNode) {
                updateMacro(macroNode, "Auto");
            });
        }
    });

    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('BlockGuidelineDo', function(event, macroNode) {
                updateMacro(macroNode, "Do");
            });
        }
    });

    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('BlockGuidelineConsider', function(event, macroNode) {
                updateMacro(macroNode, "Consider");
            });
        }
    });

    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('BlockGuidelineAvoid', function(event, macroNode) {
                updateMacro(macroNode, "Avoid");
            });
        }
    });

    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('BlockGuidelineDoNot', function(event, macroNode) {
                updateMacro(macroNode, "Do Not");
            });
        }
    });
})();

(function() {
    var updateMacro = function(macroNode, param) {
        var $macroDiv = jQuery(macroNode);
        AJS.Rte.getEditor().selection.select($macroDiv[0]);
        AJS.Rte.BookmarkManager.storeBookmark();

        var currentParams = {};
        if ($macroDiv.attr("data-macro-parameters")) {
            jQuery.each($macroDiv.attr("data-macro-parameters").split("|"), function(idx, item) {
                var param = item.split("=");
                currentParams[param[0]] = param[1];
            });
        }

        currentParams["guidance"] = param;

        var macroRenderRequest = {
            contentId: Confluence.Editor.getContentId(),
            macro: {
                name: "lp-guideline",
                params: currentParams,
                defaultParameterValue: "",
                body : $macroDiv.find(".wysiwyg-macro-body").html()
            }
        };

        tinymce.confluence.MacroUtils.insertMacro(macroRenderRequest);
    };

    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('LpGuidelineAuto', function(event, macroNode) {
                updateMacro(macroNode, "Auto");
            });
        }
    });

    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('LpGuidelineDo', function(event, macroNode) {
                updateMacro(macroNode, "Do");
            });
        }
    });

    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('LpGuidelineConsider', function(event, macroNode) {
                updateMacro(macroNode, "Consider");
            });
        }
    });

    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('LpGuidelineAvoid', function(event, macroNode) {
                updateMacro(macroNode, "Avoid");
            });
        }
    });

    AJS.bind('add-handler.property-panel', function(event, panel) {
        if (panel.registerButtonHandler) {
            panel.registerButtonHandler('LpGuidelineDoNot', function(event, macroNode) {
                updateMacro(macroNode, "Do Not");
            });
        }
    });
})();
