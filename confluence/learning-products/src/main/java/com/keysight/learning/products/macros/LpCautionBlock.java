package com.keysight.learning.products.macros;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugin.services.VelocityHelperService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LpCautionBlock extends LpHighlight
{
    private static final Logger log = LoggerFactory.getLogger(LpCautionBlock.class);

    public LpCautionBlock( VelocityHelperService velocityHelperService )
    {
       super( velocityHelperService );
    }
    @Override
    protected String getIconColorClass(){ return "keysight-lp-icon-yellow-background"; }

    @Override
    protected String getIconText(){ return "CAUTION"; }

    @Override
    protected String getColorClass(){ return "keysight-lp-yellow-background"; }

    @Override
    protected String getImage(){ return "caution.png"; }

    @Override
    protected String getLpTag(){ return "@LP_CAUTION_START@"; }
}
