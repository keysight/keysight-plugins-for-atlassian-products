package com.keysight.learning.products.macros;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.keysight.learning.products.helpers.LpHelper;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.macro.BaseMacro;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LpTaggedContent extends BaseMacro implements Macro
{
    private static final Logger log = LoggerFactory.getLogger(LpTaggedContent.class);

    private final static String BODY          = "body";
    private final static String TAG_NAME      = "tag-name";
    private final static String ENABLE_LP_TAG = "enableLpTag";

    private final VelocityHelperService velocityHelperService;

    public LpTaggedContent( VelocityHelperService velocityHelperService )
    {
       this.velocityHelperService = velocityHelperService;
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context)
            throws MacroExecutionException
    {
        String template = "/com/keysight/learning-products/templates/lp-tagged-content.vm";
        Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();

        if( parameters.containsKey( TAG_NAME ) ){
            velocityContext.put( TAG_NAME, parameters.get( TAG_NAME ) );
        }

	    if( LpHelper.IsLpExport( context ) ){
           velocityContext.put( ENABLE_LP_TAG, "true");
	    }

        velocityContext.put( BODY, body );
        return velocityHelperService.getRenderedTemplate(template, velocityContext);
    }

    @Override
    public BodyType getBodyType()
    {
        return BodyType.RICH_TEXT;
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    public boolean hasBody()
    {
        return true;
    }

    // Used to work with wiki syntax mode.
    public RenderMode getBodyRenderMode()
    {
        return RenderMode.ALL;
    }

    public String execute(Map params, String body, RenderContext renderContext) throws MacroException
    {
        try
        {
            return execute(params, body, new DefaultConversionContext( renderContext ));
        }
        catch(MacroExecutionException e)
        {
            throw new MacroException(e);
        }
    }
}
