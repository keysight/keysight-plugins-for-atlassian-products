package com.keysight.learning.products.helpers;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LpHelper
{
    private static final Logger log = LoggerFactory.getLogger(LpHelper.class);

    public static boolean IsLpExport(ConversionContext context)
    {
        boolean bIsLpExport = false;
	// Note, scroll office output to Word using the display type of pdf
	if( context.getOutputType().matches( "(word|pdf)" ) ){
            bIsLpExport = true;
	}
	return bIsLpExport;
    }
}
