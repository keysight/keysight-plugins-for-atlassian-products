package com.keysight.database.helpers;

import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DriverDetails {
   private static final Logger log = LoggerFactory.getLogger(DriverDetails.class);

   public String className;
   public boolean isLocal;
   public boolean isOk ;
   public URL url;
}
