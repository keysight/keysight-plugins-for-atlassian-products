package com.keysight.database.helpers;

import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.xhtml.api.XhtmlContent;

import javax.xml.stream.XMLStreamException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatabaseVelocityHelper
{
    private static final Logger log = LoggerFactory.getLogger(DatabaseVelocityHelper.class);

    private XhtmlContent xhtmlContent;

    @HtmlSafe
    public String convertStorageToView(String storage)
    {
        Page page = new Page();
        String view = "";
        try
        {
            final ConversionContext conversionContext = new DefaultConversionContext(page.toPageContext());
            view = xhtmlContent.convertStorageToView(storage, conversionContext);
        }
        catch (XhtmlException | XMLStreamException e)
        {
            e.printStackTrace();
        }
        return view;
    }

    @HtmlSafe
    public String htmlSafe(String html)
    {
        return html;
    }

    public void setXhtmlContent(XhtmlContent xhtmlContent) {
        this.xhtmlContent = xhtmlContent;
    }
}