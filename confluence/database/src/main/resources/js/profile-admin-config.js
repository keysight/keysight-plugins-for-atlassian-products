profilesConfigHelper = (function(jQuery) {

    var methods = new Object();
    var url = AJS.contextPath() + "/rest/database/1.0/admin-config/profiles";
    // This dictates whether the stored xml is still valid.
    // Follows the Semver standard (First digit indicates breaking change).
    var schemaVersionString = "1.1.0";
    var schemaVersionArr = schemaVersionString.split(".");
    var failedToParseSavedData = false;

    methods['disableSaveAsPriorSavedDataCannotBeLoaded'] = function(exception) {
        alert( "We do apologize.  Something went wrong parsing the saved configurations. "
            + "The ability to save had been turned off to prevent destroying "
            + "the already saved data. You might want to try Chrome as some versions "
            + "of IE have this problem.  We have spent quite a bit of time to get IE "
            + "to work correctly and for now have given up. Exception Message " + exception);

        jQuery(".update-profile-button").unbind("click");
        jQuery(".update-profile-button").click(function(e) {
            e.preventDefault();
        });
    }

    methods['canParseSavedResults'] = function(){
       var bFlag = false;
       try {
          var testXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                      + "<root><version>1.0.0</version></root>"
          var $xml = jQuery(jQuery.parseXML(testXml));
          var version = $xml.find("version").html();
          if( version === "1.0.0" ){
             failedToParseSavedData = true;
             bFlag = true;
          }
       } catch( exception ){
       }
       return bFlag;
    }

    methods['deleteProfile'] = function(profileId){
        if( failedToParseSavedData ){
           return true;
        }
        jQuery("#connection-profile-" + profileId).remove();
        saveConfig(null, true);
    }

    methods['testProfile'] = function(profileId){
        var profile = {
            "profileName": (jQuery("#profile-name-" + profileId).val()),
            "profileId": (jQuery("#view-profile-id-" + profileId).html()),
            "profileDescription": (jQuery("#profile-description-" + profileId).val()),
            "authorizedUsers": (jQuery("#authorized-users-" + profileId).val()),
            "authorizedGroups": (jQuery("#authorized-groups-" + profileId).val()),
            "authorizedSpaces": (jQuery("#authorized-spaces-" + profileId).val()),
            "databaseType": (jQuery("#database-type-" + profileId).val()),
            "databaseName": (jQuery("#database-name-" + profileId).val()),
            "databaseServer": (jQuery("#database-server-" + profileId).val()),
            "databasePort": (jQuery("#database-port-" + profileId).val()),
            "databaseUsername": (jQuery("#database-username-" + profileId).val()),
            "databasePassword": (jQuery("#database-password-" + profileId).val()),
            "connectionStringSuffix": (jQuery("#connection-string-suffix-" + profileId).val())
        }

        jQuery.ajax({
            url: url + "/test/",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(profile),
            processData: false
        }).done(function(res) {
            if (res == true) {
                AJS.messages.success("#aui-message-bar-"+profileId, {
                    title: 'Connection test succeeded.',
                    body: '<p>Connection was established with specified database.</p>',
                    fadeout: true,
                    delay: 3000
                });
            } else {
                AJS.messages.error("#aui-message-bar-"+profileId, {
                    title: 'Connection test failed.',
                    body: '<p>Couldn\'t connect for an unknown reason.</p>',
                    fadeout: true,
                    delay: 4000
                });
            }
        })
        .fail(function(self, status, error) {
            AJS.messages.warning("#aui-message-bar-"+profileId, {
                title: 'Connection test failed.',
                body: '<p>' + self.responseText + '</p>'
            });
        });
    }

    methods['updateProfile'] = function(profileId) {
        if( failedToParseSavedData ){
           return true;
        }

        jQuery("#view-profile-name-" + profileId).html(jQuery("#profile-name-" + profileId).val());
        jQuery("#view-profile-description-" + profileId).html(jQuery("#profile-description-" + profileId).val());
        jQuery("#view-authorized-users-" + profileId).html(jQuery("#authorized-users-" + profileId).val());
        jQuery("#view-authorized-groups-" + profileId).html(jQuery("#authorized-groups-" + profileId).val());
        jQuery("#view-authorized-spaces-" + profileId).html(jQuery("#authorized-spaces-" + profileId).val());
        jQuery("#view-database-type-" + profileId).html(jQuery("#database-type-" + profileId).val());
        jQuery("#view-database-name-" + profileId).html(jQuery("#database-name-" + profileId).val());
        jQuery("#view-database-server-" + profileId).html(jQuery("#database-server-" + profileId).val());
        jQuery("#view-database-port-" + profileId).html(jQuery("#database-port-" + profileId).val());
        jQuery("#view-database-username-" + profileId).html(jQuery("#database-username-" + profileId).val());
        jQuery("#view-database-password-" + profileId).html("*********");
        jQuery("#view-connection-string-suffix-" + profileId).html(jQuery("#connection-string-suffix-" + profileId).val());
        jQuery("#view-connection-string-" + profileId).html(generateConnectionStringForProfileId(profileId));
        saveConfig(profileId);
    }

    function generateConnectionStringForProfileId(profileId) {
        var connectionString = "";
        if (jQuery("#database-type-" + profileId).val() != null &&
            jQuery("#database-server-" + profileId).val() != null &&
            jQuery("#database-name-" + profileId).val() != null) {

            var subProtocol            = getDatabaseProtocolName(jQuery("#database-type-" + profileId).val());
            var port                   = jQuery("#database-port-" + profileId).val();
            var server                 = jQuery("#database-server-" + profileId).val();
            var database               = jQuery("#database-name-" + profileId).val();
            var connectionStringSuffix = jQuery("#connection-string-suffix-" + profileId).val();

            connectionString = generateConnectionString( subProtocol, port, server, database, connectionStringSuffix  )
        }
        return connectionString;

    }

    function generateConnectionString( subProtocol, port, server, database, connectionStringSuffix ) {
        var oraclePort = "1521";
        var connectionString = "";

        if ( !isEmpty( subProtocol ) && !isEmpty( server ) && !isEmpty( database ) ){
             if( subProtocol == "oracle"){
                 if (isNumber( port )) {
                     oraclePort = port;
                 }

                 connectionString = "jdbc:" + subProtocol + ":thin:"
                                  + "@" + server + ":"
                                  + oraclePort + "/"
                                  + database;
             } else {
                 connectionString = "jdbc:" + subProtocol + "://" + server;
                 if (isNumber(port)){
                     connectionString += ":" + port;
                 }

                 if (subProtocol == "sqlserver") {
                     connectionString += ";databaseName=" + database;
                 } else {
                     connectionString += "/" + database;
                 }
             }

             if ( !isEmpty( connectionStringSuffix ) ){
                 connectionString += "?" + connectionStringSuffix;
             }
        }

        return connectionString;
    }

    function getDatabaseProtocolName(databaseType) {
        var protocolName = databaseType.toLowerCase();
        if (protocolName === "mongodb") {
            protocolName = "mongo";
        } else if (protocolName === "microsoft sql server") {
            protocolName = "sqlserver";
        } else if (protocolName === "jtds sql server") {
            protocolName = "jtds:sqlserver";
        }
        return protocolName;
    }

    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function isEmpty( inputString )
    {
       var flag = false;
       if( inputString == null || inputString == "" ){
          flag = true;
       }
       return flag;
    }

    methods['addConnectionProfile'] = function(e) {
        if( failedToParseSavedData ){
           return true;
        }
        var id = makeRandomViewId();
        var profileName = "";
        var profileId = id;
        var profileDescription = "";
        var authorizedUsers = "";
        var authorizedGroups = "";
        var authorizedSpaces = "";
        var databaseType = "";
        var databaseName = "";
        var databaseServer = "";
        var databasePort = "";
        var databaseUsername = "";
        var databasePassword = "";
        var connectionStringSuffix = "";
        var db2Selected = "";
        var derbySelected = "";
        var microsoftSqlServerSelected = "";
        var jTdsSqlServerSelected = "";
        var mongoDbSelected = "";
        var mySqlSelected = "";
        var oracleSelected = "";
        var postgreSqlSelected = "";
        var sybaseSelected = "";
        var connectionString = "";

        jQuery("#connection-profiles").append(Keysight.Database.Admin.Config.Templates.connectionProfile({
            id: id,
            profileId: profileId,
            profileName: profileName,
            profileDescription: profileDescription,
            authorizedUsers: authorizedUsers,
            authorizedGroups: authorizedGroups,
            authorizedSpaces: authorizedSpaces,
            databaseType: databaseType,
            databaseName: databaseName,
            databaseServer: databaseServer,
            databasePort: databasePort,
            databaseUsername: databaseUsername,
            databasePassword: databasePassword,
            connectionStringSuffix: connectionStringSuffix,
            db2Selected: db2Selected,
            derbySelected: derbySelected,
            jTdsSqlServerSelected: jTdsSqlServerSelected,
            microsoftSqlServerSelected: microsoftSqlServerSelected,
            mongoDbSelected: mongoDbSelected,
            mySqlSelected: mySqlSelected,
            oracleSelected: oracleSelected,
            postgreSqlSelected: postgreSqlSelected,
            sybaseSelected: sybaseSelected,
            connectionString: connectionString,
        }));

        AJS.tabs.setup();
        AJS.tabs.change(jQuery('a[href="#edit-' + id + '"]'));
        bindButtons();
    }

    methods['removeEntry'] = function(e) {
        e.preventDefault();
        if( failedToParseSavedData ){
           return true;
        }
        jQuery(e.currentTarget).parent().remove();
        saveConfig(null, true);
    }


    methods['loadConfig'] = function() {
        jQuery.ajax({
            url: url,
            dataType: "json"
        }).done(function(pluginConfiguration) {
            if (pluginConfiguration.xml == null) {
                return;
            }
            failedToParseSavedData = false;

            try{
                var jdbcUrl = AJS.contextPath() + "/rest/database/1.0/admin-config/get-installed-drivers";

                try {
                    $xml = jQuery(jQuery.parseXML(decodeURIComponent(pluginConfiguration.xml)));
                } catch (error) {
                    console.error("Failed to parse XML: ", error);
                    $xml = jQuery(jQuery.parseXML(
                        "<plugin-configuration>\n" +
                        "    <saved-profiles></saved-profiles>\n" +
                        "</plugin-configuration>"
                    ));
                }

                // Check for breaking changes in the schema
                // No breaking change occured when we added the schema version, so if one doesn't exist the xml
                // is still valid.
                if ($xml.find("schema-version").length != 0) {
                    var xmlVersion = $xml.find("schema-version").html();
                    var xmlMajorVer = xmlVersion.split(".")[0];
                    if (xmlMajorVer != schemaVersionArr[0]) {
                        $xml = jQuery(jQuery.parseXML(
                            "<plugin-configuration>\n" +
                            "    <saved-profiles></saved-profiles>\n" +
                            "</plugin-configuration>"
                        ));

                    AJS.messages.error({
                            title: 'Error fetching profiles.',
                            body: '<p>The stored profiles data is no longer compatible with this version of the plugin.</p>' +
                                '<p>If you save on this page, all old profiles will be lost. To preserve them, roll back the plugin ' +
                            'version and then make a copy of the profile information, and then reenter it after upgrading.</p>'
                        });
                    }
                }

                // Load saved connection profiles in and connect them to templates
                $xml.find("saved-profile").each(function(index) {
                    var id = makeRandomViewId();
                    var profileId = atob(jQuery(this).find("profile-id").html());
                    var profileName = atob(jQuery(this).find("profile-name").html());
                    var profileDescription = atob(jQuery(this).find("profile-description").html());
                    var authorizedUsers = atob(jQuery(this).find("authorized-users").html());
                    var authorizedGroups = atob(jQuery(this).find("authorized-groups").html());
                    var databaseType = atob(jQuery(this).find("database-type").html());
                    var databaseName = atob(jQuery(this).find("database-name").html());
                    var databaseServer = atob(jQuery(this).find("database-server").html());
                    var databasePort = atob(jQuery(this).find("database-port").html());
                    var databaseUsername = atob(jQuery(this).find("database-username").html());
                    var databasePassword = "*****";
                    var connectionStringSuffix = atob(jQuery(this).find("connection-string-suffix").html());
                    var db2Selected = "";
                    var derbySelected = "";
                    var jTdsSqlServerSelected = "";
                    var microsoftSqlServerSelected = "";
                    var mongoDbSelected = "";
                    var mySqlSelected = "";
                    var oracleSelected = "";
                    var postgreSqlSelected = "";
                    var sybaseSelected = "";

                    var authorizedSpaces = "";
                    var authorizedSpacesElement = jQuery(this).find("authorized-spaces");
                    if(authorizedSpacesElement.length > 0)
                    {
                        authorizedSpaces = atob(authorizedSpacesElement.html());
                    }

                    if (databaseType == "DB2") {
                        db2Selected = "selected";
                    } else if (databaseType == "Derby") {
                        derbySelected = "selected";
                    } else if (databaseType == "jTDS SQL Server") {
                        jTdsSqlServerSelected = "selected";
                    } else if (databaseType == "Microsoft SQL Server") {
                        microsoftSqlServerSelected = "selected";
                    } else if (databaseType == "MongoDB") {
                        mongoDbSelected = "selected";
                    } else if (databaseType == "MySQL") {
                        mySqlSelected = "selected";
                    } else if (databaseType == "Oracle") {
                        oracleSelected = "selected";
                    } else if (databaseType == "PostgreSQL") {
                        postgreSqlSelected = "selected";
                    } else if (databaseType == "Sybase") {
                        sybaseSelected = "selected";
                    }

                    connectionString = generateConnectionString( getDatabaseProtocolName( databaseType ),
                                                                 databasePort,
                                                                 databaseServer,
                                                                 databaseName,
                                                                 connectionStringSuffix );

                    jQuery("#connection-profiles").append(Keysight.Database.Admin.Config.Templates.connectionProfile({
                        id: id,
                        profileName: profileName,
                        profileId: profileId,
                        profileDescription: profileDescription,
                        authorizedUsers: authorizedUsers,
                        authorizedGroups: authorizedGroups,
                        authorizedSpaces: authorizedSpaces,
                        databaseType: databaseType,
                        databaseName: databaseName,
                        databaseServer: databaseServer,
                        databasePort: databasePort,
                        databaseUsername: databaseUsername,
                        databasePassword: databasePassword,
                        connectionStringSuffix: connectionStringSuffix,
                        db2Selected: db2Selected,
                        derbySelected: derbySelected,
                        jTdsSqlServerSelected: jTdsSqlServerSelected,
                        microsoftSqlServerSelected: microsoftSqlServerSelected,
                        mongoDbSelected: mongoDbSelected,
                        mySqlSelected: mySqlSelected,
                        oracleSelected: oracleSelected,
                        postgreSqlSelected: postgreSqlSelected,
                        sybaseSelected: sybaseSelected,
                        connectionString: connectionString,
                    }));
                });

                jQuery("#save-config-auth").click(function(e) {
                    e.preventDefault();
                    profilesConfigHelper.saveConfigAuth();
                });

                AJS.tabs.setup();
                bindButtons();

            } catch( exception ){
               failedToParseSavedData = true;
               // for some reason calling this.disableSaveAsPriorSavedDataCannotBeLoaded(exception); fails.
               alert( "We do apologize.  Something went wrong parsing the saved configurations. "
                    + "The ability to save had been turned off to prevent destroying "
                    + "the already saved data. You might want to try Chrome as some versions "
                    + "of IE have this problem.  We have spent quite a bit of time to get IE "
                    + "to work correctly and for now have given up. Exception Messaage " + exception);

                jQuery(".update-profile-button").unbind("click");
                jQuery(".update-profile-button").click(function(e) {
                    e.preventDefault();
                });
            }
        })
        .fail(function(self, status, error) {
            console.error(error);
            AJS.messages.error({
                title: 'Error fetching profiles.',
                body: 'Internal Error: Please contact your Confluence administrators.'
            });
        });
    }

    function saveConfig(savedId, isDeletion) {
        if( failedToParseSavedData ){
           return true;
        }
        var savedProfiles = new Array();
        var profileAuth = {};
        var profileName = "";

        try {
            jQuery(".edit-connection-profile").each(function(index) {
                var profileId = jQuery(this).attr("connection-profile-id");

                // If password hasn't changed, don't send it
                // Only update the password for the one we changed.
                if (profileId == savedId) {
                    var password = jQuery("#database-password-" + profileId).val();
                    if (password != "*****") {
                        profileAuth[jQuery("#view-profile-id-" + profileId).html()] = password;
                    }
                }

                // If profile has the same name as a previous profile, die and show error.
                var newProfileName = jQuery("#view-profile-name-" + profileId).html();
                if (newProfileName == profileName) {
                    throw "Canceling Save";
                } else {
                    profileName = newProfileName;
                }

                // We look at the #view-... tags here because only the profile we hit save on will update the view page
                // The others should be unchanged even if the user entered data.
                savedProfiles.push("      <saved-profile>\n" +
                        "         <profile-name>" + btoa(jQuery("#view-profile-name-" + profileId).html()) + "</profile-name>\n" +
                        "         <profile-id>" + btoa( jQuery("#view-profile-id-" + profileId).html() ) + "</profile-id>\n" +
                        "         <profile-description>" + btoa(jQuery("#view-profile-description-" + profileId).html()) + "</profile-description>\n" +
                        "         <authorized-users>" + btoa(jQuery("#view-authorized-users-" + profileId).html()) + "</authorized-users>\n" +
                        "         <authorized-groups>" + btoa(jQuery("#view-authorized-groups-" + profileId).html()) + "</authorized-groups>\n" +
                        "         <authorized-spaces>" + btoa(jQuery("#view-authorized-spaces-" + profileId).html()) + "</authorized-spaces>\n" +
                        "         <database-type>" + btoa(jQuery("#view-database-type-" + profileId).html()) + "</database-type>\n" +
                        "         <database-name>" + btoa(jQuery("#view-database-name-" + profileId).html()) + "</database-name>\n" +
                        "         <database-server>" + btoa(jQuery("#view-database-server-" + profileId).html()) + "</database-server>\n" +
                        "         <database-port>" + btoa(jQuery("#view-database-port-" + profileId).html()) + "</database-port>\n" +
                        "         <database-username>" + btoa(jQuery("#view-database-username-" + profileId).html()) + "</database-username>\n" +
                        "         <database-password>" + "*****" + "</database-password>\n" +
                        "         <connection-string-suffix>" + btoa(jQuery("#view-connection-string-suffix-" + profileId).html()) + "</connection-string-suffix>\n" +
                        "      </saved-profile>\n");
            });
        } catch (error) {
            AJS.messages.error("#aui-message-bar-"+savedId, {
                title: 'Error saving profiles.',
                body: '<p> Two profiles cannot share the same name. Please change the name of one of your profiles.</p>',
                fadeout: true
            });
            return;
        }

        var xmlString = '<?xml version="1.0" encoding="UTF-8"?>' + "\n" +
            '<plugin-configuration>' + "\n" +
            '<schema-version>' + schemaVersionString + '</schema-version>' + "\n" +
            '   <saved-profiles>' + "\n" +
            savedProfiles.join("\n") +
            '   </saved-profiles>' + "\n" +
            '</plugin-configuration>' + "\n";

        // Validate XML
        try {
            jQuery(jQuery.parseXML(xmlString))
        } catch (error) {
            AJS.messages.error("#aui-message-bar-"+savedId, {
                title: 'Error saving profiles.',
                body: '<p> Internal Error: Please contact your Confluence administrators.</p>'
            });
            console.error("Malformed XML!: ", error);
            return;
        }


        jQuery.ajax({
            url: url,
            type: "PUT",
            contentType: "application/json",
            data: '{"xml":"' + encodeURIComponent(xmlString) + '", "passwordMap":' + JSON.stringify(profileAuth) + '}',
            processData: false
        }).done(function() {
            var saveMsg;
            if (isDeletion) {
                saveMsg = "<p> You have successfully deleted this profile.</p>";
                AJS.messages.success("#aui-message-bar", {
                    title: 'Success!',
                    body: saveMsg,
                    fadeout: true,
                    delay: 2000
                });
            } else {
                AJS.tabs.change(jQuery('a[href="#view-' + savedId + '"]'));
                saveMsg = "<p> You have successfully saved this profile.</p>";
                AJS.messages.success("#aui-message-bar-"+savedId, {
                    title: 'Success!',
                    body: saveMsg,
                    fadeout: true,
                    delay: 2000
                });
            }
        }).fail(function(self, status, error) {
            AJS.messages.error("#aui-message-bar-"+savedId, {
                title: 'Error saving profiles.',
                body: '<p>Internal Error: Please contact your Confluence administrators.</p>'
            });
            console.error("Bad Request!: ", error);
        });
    }

    function bindButtons() {
        jQuery(".update-profile-button").unbind("click");
        jQuery(".update-profile-button").click(function(e) {
            e.preventDefault();
            profilesConfigHelper.updateProfile(jQuery(e.target).attr("connection-profile"));
        });

        jQuery(".delete-profile-button").unbind("click");
        jQuery(".delete-profile-button").click(function(e) {
            e.preventDefault();
            profilesConfigHelper.deleteProfile(jQuery(e.target).attr("connection-profile"));
        });

        jQuery(".test-profile-button").unbind("click");
        jQuery(".test-profile-button").click(function(e) {
            e.preventDefault();
            profilesConfigHelper.testProfile(jQuery(e.target).attr("connection-profile"));
        });
    }

    function makeRandomViewId() {
        var size = 10000000;
        var number = Math.floor((Math.random() * size) + 1);
        while (idExists("view-" + number)) {
            number = Math.floor((Math.random() * size) + 1);
        }
        return number;
    }

    function idExists(id) {
        var flag = false;
        if (jQuery("#" + id).length != 0) {
            flag = true;
        }
        return flag;
    }

    return methods;
})(jQuery);

AJS.toInit(function() {
    if( profilesConfigHelper.canParseSavedResults() ){
        jQuery("#add-connection-profile").click(function(e) {
            e.preventDefault();
            profilesConfigHelper.addConnectionProfile();
        });

        jQuery(".update-profile-button").unbind("click");
        jQuery(".update-profile-button").click(function(e) {
            e.preventDefault();
            profilesConfigHelper.updateProfile(jQuery(e.target).attr("connection-profile"));
        });

        profilesConfigHelper.loadConfig();
    } else {
        profilesConfigHelper.disableSaveAsPriorSavedDataCannotBeLoaded();
    }

    // Without this hook, Confluence will scroll the page
    // so that the top of the tab is at the top of the screen.
    // This code triggers the tab change, than stops propagation
    // or the event so the page is not scrolled.
    AJS.tabs.setup();
    jQuery(".keysight-tab-menu-item-anchor").click(function(e){
       AJS.tabs.change(jQuery(this), e)
       e.preventDefault()
       e.stopImmediatePropagation();
    })
});