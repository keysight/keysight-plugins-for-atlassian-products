package com.keysight.mathjax.macros;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.v2.components.HtmlEscaper;
import com.keysight.mathjax.helpers.PluginHelper;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.macro.BaseMacro;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MathJaxBlock extends BaseMacro implements Macro {
    private static final Logger log = LoggerFactory.getLogger(MathJaxBlock.class);

    public final static String EQUATION = "equation";
    public final static String BODY_AS_HTML = "bodyAsHtml";
    protected final VelocityHelperService velocityHelperService;
    protected final XhtmlContent xhtmlUtils;
    protected final PluginHelper pluginHelper;

    public MathJaxBlock(PluginHelper pluginHelper,
                        VelocityHelperService velocityHelperService,
                        XhtmlContent xhtmlUtils) {
        this.pluginHelper = pluginHelper;
        this.velocityHelperService = velocityHelperService;
        this.xhtmlUtils = xhtmlUtils;
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context)
            throws MacroExecutionException {
        String template = "/com/keysight/mathjax/templates/mathjax-block.vm";
        Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();

        log.debug("The body of the mathjax content is: " + body);
        log.debug("The html escaped body of the mathjax content is: " + HtmlEscaper.escapeAll(body, true));
        if (body.length() == 0) {
            body = "No equation provided";
        } else {
            body = pluginHelper.getBlockMathjaxStartIdentifier() + HtmlEscaper.escapeAll(body, true) + pluginHelper.getBlockMathjaxEndIdentifier();
        }

        velocityContext.put(BODY_AS_HTML, body);

        return velocityHelperService.getRenderedTemplate(template, velocityContext);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

    // Used to work with wiki syntax mode.
    public boolean hasBody()
    {
        return true;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.ALL;
    }

    public String execute(Map params, String body, RenderContext renderContext) throws MacroException
    {
        try
        {
            if (body.startsWith("<p>"))
            {
                body = body.substring(3);
            }

            if (body.endsWith("</p>"))
            {
                body = body.substring(0, body.length()-4);
            }

            return execute(params, body, new DefaultConversionContext( renderContext ));
        }
        catch(MacroExecutionException e)
        {
            throw new MacroException(e);
        }
    }
}
