package com.keysight.mathjax.helpers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Class used to contain shared static configuration details
abstract class Constants {
    private static final Logger log = LoggerFactory.getLogger(Constants.class);

    // The url to used to serve Mathjax before the user provides one
    static String DEFAULT_URL = "https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg-full.js";
    static String MATHJAX_1_DEFAULT_URL = "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML";
    static String DEFAULT_INLINE_MATHJAX_START_IDENTIFIER = "(mathjax-inline(";
    static String DEFAULT_INLINE_MATHJAX_END_IDENTIFIER = ")mathjax-inline)";
    static String DEFAULT_BLOCK_MATHJAX_START_IDENTIFIER = "(mathjax-block(";
    static String DEFAULT_BLOCK_MATHJAX_END_IDENTIFIER = ")mathjax-block)";
    static String DEFAULT_MATHJAX_ASCII_MATH_START_IDENTIFIER = "(mathjax-ascii-math(";
    static String DEFAULT_MATHJAX_ASCII_MATH_END_IDENTIFIER = ")mathjax-ascii-math)";
}